{**
 * header.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site header.
 *
 * $Id: header.tpl,v 1.26.2.1 2009/04/08 20:45:51 asmecher Exp $
 *}
{if !$pageTitleTranslated}{translate|assign:"pageTitleTranslated" key=$pageTitle}{/if}
{if $pageCrumbTitle}{translate|assign:"pageCrumbTitleTranslated" key=$pageCrumbTitle}{elseif !$pageCrumbTitleTranslated}{assign var="pageCrumbTitleTranslated" value=$pageTitleTranslated}
{/if}
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset={$defaultCharset|escape}" />
	<title>{$pageTitleTranslated}</title>
	<meta name="description" content="{$metaSearchDescription|escape}" />
	<meta name="keywords" content="{$metaSearchKeywords|escape}" />
	<meta name="generator" content="{translate key="common.openConferenceSystems"} {$currentVersionString|escape}" />

	{$metaCustomHeaders}

    {assign var="lastUpdateCss" value="?2010-04-25"}
    <link href="{$baseUrl}/css/grid.css{$lastUpdateCss}" rel="stylesheet" type="text/css" />
    <link href="{$baseUrl}/css/template.css{$lastUpdateCss}" rel="stylesheet" type="text/css" />
    <link href="{$baseUrl}/css/index.css{$lastUpdateCss}" rel="stylesheet" type="text/css" />
    <link href="{$baseUrl}/css/news.css{$lastUpdateCss}" rel="stylesheet" type="text/css" />
	{foreach from=$stylesheets item=cssUrl}
	<link rel="stylesheet" href="{$cssUrl}" type="text/css" />
	{/foreach}
	{*
	<link rel="stylesheet" href="{$baseUrl}/styles/common.css" type="text/css" />
	*}

	{*
	{call_hook|assign:"leftSidebarCode" name="Templates::Common::LeftSidebar"}
	{call_hook|assign:"rightSidebarCode" name="Templates::Common::RightSidebar"}
	{if $leftSidebarCode || $rightSidebarCode}<link rel="stylesheet" href="{$baseUrl}/styles/sidebar.css" type="text/css" />{/if}
	{if $leftSidebarCode}<link rel="stylesheet" href="{$baseUrl}/styles/leftSidebar.css" type="text/css" />{/if}
	{if $rightSidebarCode}<link rel="stylesheet" href="{$baseUrl}/styles/rightSidebar.css" type="text/css" />{/if}
	{if $leftSidebarCode && $rightSidebarCode}<link rel="stylesheet" href="{$baseUrl}/styles/bothSidebars.css" type="text/css" />{/if}

	*}
	<script type="text/javascript" src="{$baseUrl}/js/general.js"></script>
    {foreach from=$javascripts item=jsUrl name=wsfoo}
	<script type="text/javascript" src="{$jsUrl}"></script>
	{/foreach}
	{$smart.foreach.wsfoo.total}
	
	{$additionalHeadData}
</head>
<body>

    <div id="topBar">
        <div class="container_16">
            <ul class="grid_16">
                {*
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">News</a></li>
                <li><a href="#">Registration</a></li>
                <li><a href="#">Events</a></li>
                <li><a href="#">Schedule</a></li>
                <li><a href="#">Keynotes</a></li>
                <li><a href="#">Accommodation</a></li>
                <li><a href="#">Sponsors</a></li>
                *}
                
		        <li><a href="{url conference="index" schedConf="index" op="index"}">Home</a></li>
		        <li><a href="{$baseUrl}/index.php/guadec/2010/">The conference</a></li>
		        
		        {*
		        
	            <!-- We don't want to have a search page in Guadec website -->
		        <li><a href="{url page="search"}">{translate key="navigation.search"}</a></li>
		        
		        *}
		        
		        {if $currentConference}
			        
			        {*{if $currentSchedConfsExist}
			        <li><a href="{url schedConf="index" page="schedConfs" op="current"}">{translate key="navigation.current"}</a></li>
			        {/if}
			        
			        {if $archivedSchedConfsExist}
			        <li><a href="{url schedConf="index" page="schedConfs" op="archive"}">{translate key="navigation.archive"}</a></li>
			        {/if}*}
			        
			        {if $enableAnnouncements}
				        <li><a href="{url page="announcement"}">{translate key="announcement.announcements"}</a></li>
			        {/if}
			        
			        {call_hook name="Templates::Common::Header::Navbar::CurrentConference"}
			        
		        {/if}
		        
		        <li><a href="http://live.gnome.org/GUADEC/2010">Wiki</a></li>
		        <li><a href="https://register.guadec.org/">Registration</a></li>
		        
		        {if $isUserLoggedIn}
			        <li><a href="{url page="user"}">{translate key="navigation.userHome"}</a></li>
		        {else}
			        <li><a href="{url page="login"}">{translate key="navigation.login"}</a></li>
			        {*<li><a href="{url page="user" op="account"}">{translate key="navigation.account"}</a></li>*}
		        {/if}
		        
		        {foreach from=$navMenuItems item=navItem}
			        <li><a href="{if $navItem.isAbsolute}{$navItem.url|escape}{else}{$navItem.url|escape}{/if}">{if $navItem.isLiteral}{$navItem.name|escape}{else}{translate key=$navItem.name}{/if}</a></li>
		        {/foreach}
            </ul>
            <div class="clear"></div>
        </div>
    </div>
    
    <div id="header"{if $pageId == 'conference.index'} class="index"{/if}>
        <div class="container_16">
            <div class="grid_16">
                {if $pageId == 'conference.index'}
                    <h1><a href="{$baseUrl}"><img src="{$baseUrl}/img/index-logo.png" alt="GUADEC, The GNOME Conference" /></a></h1>
                {else}
                    <h1><a href="{$baseUrl}"><img src="{$baseUrl}/img/header-logo.png" alt="GUADEC, The GNOME Conference" /></a></h1>
                {/if}
                <h2 class="silhouette">The Hague, Netherlands - July 24-30th, 2010</h2>                
            </div>
            <div class="clear"></div>
        </div>
    </div>

    {if $leftSidebarCode || $rightSidebarCode}
	<div id="sidebar">
		{if $leftSidebarCode}
			<div id="leftSidebar">
				{$leftSidebarCode}
			</div>
		{/if}
		{if $rightSidebarCode}
			<div id="rightSidebar">
				{$rightSidebarCode}
			</div>
		{/if}
	</div>
    {/if}
    
    {if $pageId == 'conference.index'}
    
    <div id="goldSponsors">
        <div class="container_16">
            <div class="grid_16">
                {*
                
                Temporarily hiding gold sponsors list
                
                <dl>
                    <dt>Gold Sponsors</dt>
                    <dd>
                        <ul>
                            <li><a href="#"><img src="{$baseUrl}/img/gold-sponsor-1.png" alt="Sponsor #1"></a></li><li><a href="#"><img src="{$baseUrl}/img/gold-sponsor-1.png" alt="Sponsor #2"></a></li><li><a href="#"><img src="{$baseUrl}/img/gold-sponsor-1.png" alt="Sponsor #3"></a></li>
                        </ul>
                    </dd>
                </dl>
                
                *}
            </div>
            <div class="clear"></div>
        </div>
    </div>

    {else}

    <div id="breadcrumb">
        <div class="container_16">
            <div class="grid_16">
	            <a href="{url conference="index" schedConf="index" page="index"}">{translate key="navigation.home"}</a> ‣
	            {foreach from=$pageHierarchy item=hierarchyLink}
	                {if $hierarchyLink[1] != 'GUADEC'}
    		            <a href="{$hierarchyLink[0]|escape}" class="hierarchyLink">{if not $hierarchyLink[2]}{translate key=$hierarchyLink[1]}{else}{$hierarchyLink[1]|escape}{/if}</a> ‣
		            {/if}
	            {/foreach}
	            <strong><a href="{$currentUrl|escape}" class="current">{$pageCrumbTitleTranslated}</a></strong>
        	</div>
        	<div class="clear"></div>
	    </div>
    </div>
    
    {/if}

    <div id="content">
        <div class="container_16">
            <div class="grid_16">
                {if !$hideTitle}
                <h1>{$pageTitleTranslated}</h1>
                {/if}
