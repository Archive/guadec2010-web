{**
 * footer.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site footer.
 *
 * $Id: footer.tpl,v 1.8.2.2 2009/04/17 06:45:47 jmacgreg Exp $
 *}
 
    </div>

    {if $postCreativeCommons}
	{translate key="common.ccLicense"}
    {/if}

    {call_hook name="Templates::Common::Footer::PageFooter"}

    {get_debug_info}
    
    
    <div id="footer">
        <div class="container_16">
            
            <div class="grid_4">
                <img src="{$baseUrl}/img/the-hague-footer-logo.png" alt="GNOME" />
            </div>
            
            <div class="grid_3">
                <ul>
                    <li><a href="{url conference="index" schedConf="index" op="index"}"><strong>Home</strong></a></li>
                    <li><a href="https://register.guadec.org/">Registration</a></li>
                    <li><a href="http://live.gnome.org/GUADEC/2010">Wiki</a></li>
                    
                    {if $isUserLoggedIn}
		                <li><a href="{url page="user"}">{translate key="navigation.userHome"}</a></li>
	                {else}
		                <li><a href="{url page="login"}">{translate key="navigation.login"}</a></li>
	                {/if}
                </ul>
            </div>
            <div class="grid_3">
                <ul>
                    <li><a href="{$baseUrl}/index.php/guadec/2010/"><strong>The Conference</strong></a></li>
                    <li><a href="{url page="announcement"}">Announcements</a></li>
                    <li><a href="{$baseUrl}/index.php/guadec/2010/schedConf/overview">Schedule</a></li>
                    <li><a href="{$baseUrl}/index.php/guadec/2010/schedConf/training">Developer Training</a></li>
                </ul>
            </div>
            <div class="grid_3">
                <ul>
                    <li><a href="{$baseUrl}/index.php/guadec/2010/schedConf/accommodation"><strong>Accomodation</strong></a></li>
                    <li><a href="{$baseUrl}/index.php/guadec/2010/schedConf/accommodation#hotels">Hotels and Hostels</a></li>
                    <li><a href="{$baseUrl}/index.php/guadec/2010/schedConf/accommodation#transportation">Transportation</a></li>
                    <li><a href="{$baseUrl}/index.php/guadec/2010/schedConf/accommodation#map">City Map</a></li>
                </ul>
            </div>
            <div class="grid_3">
                <ul>
                    <li><a href="{$baseUrl}/index.php/guadec/2010/livestream"><strong>Media Livestream</strong></a></li>
                    <li><a href="http://planet.gnome.org">Aggregated GNOME blogs</a></li>
                    <li><a href="http://identi.ca/guadec">Follow us on Identi.ca</a></li>
                    <li><a href="http://twitter.com/guadec">Follow us on Twitter</a></li>
                </ul>
            </div>
            
            <div class="copyright grid_16">
                © 2010 <a href="http://www.gnome.org/">The GNOME Project
            </div>
            
            <div class="clear"></div>
        <div>
    </div>
    
{if $enableDebugStats}
<div id="footer">
	<div id="footerContent">
		<div class="debugStats">
		{translate key="debug.executionTime"}: {$debugExecutionTime|string_format:"%.4f"}s<br />
		{translate key="debug.databaseQueries"}: {$debugNumDatabaseQueries|escape}<br/>
		{if $debugNotes}
			<strong>{translate key="debug.notes"}</strong><br/>
			{foreach from=$debugNotes item=note}
				{translate key=$note[0] params=$note[1]}<br/>
			{/foreach}
		{/if}
		</div>
	</div><!-- footerContent -->
</div><!-- footer -->
{/if}

</div><!-- container -->

{literal}
<!-- Piwik -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://webstats.gnome.org/" : "http://webstats.gnome.org/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 9);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://webstats.gnome.org/piwik.php?idsite=9" style="border:0" alt=""/></p></noscript>
<!-- End Piwik Tag -->
{/literal}

</body>
</html>
