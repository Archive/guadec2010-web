{**
 * header.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site header.
 *
 * $Id: header.tpl,v 1.26.2.1 2009/04/08 20:45:51 asmecher Exp $
 *}
{if !$pageTitleTranslated}{translate|assign:"pageTitleTranslated" key=$pageTitle}{/if}
{if $pageCrumbTitle}{translate|assign:"pageCrumbTitleTranslated" key=$pageCrumbTitle}{elseif !$pageCrumbTitleTranslated}{assign var="pageCrumbTitleTranslated" value=$pageTitleTranslated}
{/if}
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset={$defaultCharset|escape}" />
	<title>{$pageTitleTranslated}</title>
	<meta name="description" content="{$metaSearchDescription|escape}" />
	<meta name="keywords" content="{$metaSearchKeywords|escape}" />
	<meta name="generator" content="{translate key="common.openConferenceSystems"} {$currentVersionString|escape}" />

	{$metaCustomHeaders}

    {assign var="lastUpdateCss" value="?2010-04-25"}
    <link href="{$baseUrl}/css/grid.css{$lastUpdateCss}" rel="stylesheet" type="text/css" />
    <link href="{$baseUrl}/css/template.css{$lastUpdateCss}" rel="stylesheet" type="text/css" />
    <link href="{$baseUrl}/css/index.css{$lastUpdateCss}" rel="stylesheet" type="text/css" />
    <link href="{$baseUrl}/css/news.css{$lastUpdateCss}" rel="stylesheet" type="text/css" />
	{foreach from=$stylesheets item=cssUrl}
	<link rel="stylesheet" href="{$cssUrl}" type="text/css" />
	{/foreach}

	<script type="text/javascript" src="{$baseUrl}/js/general.js"></script>
    {foreach from=$javascripts item=jsUrl name=wsfoo}
	<script type="text/javascript" src="{$jsUrl}"></script>
	{/foreach}
	{$smart.foreach.wsfoo.total}
	
	{$additionalHeadData}
</head>
<body>
    <div id="content">
