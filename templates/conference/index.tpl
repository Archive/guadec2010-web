{**
 * index.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Conference index page. Displayed when a conference, but not a scheduled
 * conference, has been selected.
 *
 * $Id: index.tpl,v 1.19.2.1 2009/04/08 20:45:51 asmecher Exp $
 *}
{assign var="pageTitleTranslated" value=$conferenceTitle}
{assign var="pageId" value="conference.index"}
{assign var="hideTitle" value="true"}
{include file="common/header.tpl"}

                <div id="attention-nav">
                    <ul>
                        <li><a href="{url conference="guadec" schedConf="2010" page="schedConf" op="accommodation" anchor="venue"}" title="Venue">Venue</a></li>
                        <li><a href="{url conference="guadec" schedConf="2010" page="schedConf" op="accommodation" anchor="accommodation"}" title="Accommodation">Accommodation</a></li>
                        <li><a href="{url conference="guadec" schedConf="2010" page="livestream"}" title="Social Media Livestream">Social Media Livestream</a></li>
                        <li><a href="/docs/party_map.pdf" title"GUADEC 2010 Parties on the map">Parties Map</a></li>
                    </ul>
                    {*<p style="position:absolute; top: -40px; right: 0px; float: right;>       
                        <a href="http://register.guadec.org/" title="GUADEC 2010 registration"><img src="{$baseUrl}/img/register_now.png" alt="Register Now"/></a>
                     </p>*}
                </div>

                <div class="grid_6 alpha">
                    <h1>About GUADEC</h1>
                    
                    <p class="feature">
                        GUADEC (pronounced <em>gwah-dec</em>) is the largest get together of GNOME users, developers, foundation leaders, individuals, governments and businesses in the world.
                    <p>
                    
                    <p>GUADEC  is an acronym for the GNOME Users’ And Developers’ European Conference. Held annually in cities around Europe, presentations are given by software developers, business leaders and users who are involved in Open Source, Free Software and of course, GNOME.</p>
                    
                    <p>These combine with many practical sessions and discussion groups which set the direction of the GNOME project. It also aims to attract new developers and contributors, as well has serve as a meeting point for corporate interests and allow us to showcase some of the latest technologies.</p>
                    
                    <p>In its <strong>11th consecutive year</strong>, GUADEC 2010 is being held in the <strong><a href="http://portal.hhs.nl/portal/page/portal/en">The Hague University</a></strong>, in The Hague, Netherlands, from <strong>Monday 26th July, until Friday 30th July</strong>.</p>
                    
                    <p><a href="{url conference="guadec" schedConf="2010"}" title="GUADEC 2010"><em>More information...</em></a></p>
                    
                </div>
                
                <div class="grid_10 omega" id="frontstream">
            <h2 class="flickr"><abbr title="GNOME Users’ And Developers’ European Conference">GUADEC</abbr> 2010 on <span style="color: #3465a4;">Flick</span><span style="color: #ff048c;">r</span></h2>
                        
                        <div class="flickrPhotos">
                            <a href="http://www.flickr.com/photos/guadec2010/4840844013/"><img src="http://farm5.static.flickr.com/4104/4840844013_8cc498c80d_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4838134638/"><img src="http://farm5.static.flickr.com/4125/4838134638_2e4f816859_s.jpg" alt="" /></a>
<a href="http://www.flickr.com/photos/mariosp/4840485808/" title="Den Haag HS by mariosp, on Flickr"><img src="http://farm5.static.flickr.com/4144/4840485808_afb7742af1_s.jpg" width="75" height="75" alt="Den Haag HS" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4841460016/"><img src="http://farm5.static.flickr.com/4153/4841460016_a042befe06_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/qense/4846339972/" title="GUADEC 2010 venue at Friday morning by Sense Hofstede, on Flickr"><img src="http://farm5.static.flickr.com/4084/4846339972_9ee47bbbe1_s.jpg" alt="GUADEC 2010 venue at Friday morning" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4837522587/"><img src="http://farm5.static.flickr.com/4150/4837522587_43ac17d13f_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4837522109/"><img src="http://farm5.static.flickr.com/4087/4837522109_9d20391112_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4838133492/"><img src="http://farm5.static.flickr.com/4088/4838133492_caa4136dd2_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4837274054/"><img src="http://farm5.static.flickr.com/4113/4837274054_57232d5649.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4836662023/"><img src="http://farm5.static.flickr.com/4089/4836662023_d8fc950007_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4836659933/"><img src="http://farm5.static.flickr.com/4149/4836659933_1abca2e7c1_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4836659891/"><img src="http://farm5.static.flickr.com/4110/4836659891_cee777bb11_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4837271586/"><img src="http://farm5.static.flickr.com/4105/4837271586_e47986620e_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4837271004/"><img src="http://farm5.static.flickr.com/4130/4837271004_4e3c0e616c_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4837270196/"><img src="http://farm5.static.flickr.com/4125/4837270196_96ca428653_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4837270478/"><img src="http://farm5.static.flickr.com/4087/4837270478_67fb1f0278_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4837522723/"><img src="http://farm5.static.flickr.com/4124/4837522723_d7d8587b37_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/guadec2010/4841462332/"><img src="http://farm5.static.flickr.com/4124/4841462332_b6410c055e_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/mdkflickr/4843340635/"><img src="http://farm5.static.flickr.com/4148/4843340635_4022f37b56_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/mdkflickr/4843338611/"><img src="http://farm5.static.flickr.com/4108/4843338611_ffdc02d580_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/cmoi/4843189318/"><img src="http://farm5.static.flickr.com/4152/4843189318_c27ec55a06_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/mariosp/4840463870/" title="Lake in The Hague by mariosp, on Flickr"><img src="http://farm5.static.flickr.com/4109/4840463870_fd671d6c7d_s.jpg" alt="Lake in The Hague" /></a>
                            <a href="http://www.flickr.com/photos/mariosp/4840484462/" title="Xan and Fer's talk by mariosp, on Flickr"><img src="http://farm5.static.flickr.com/4111/4840484462_9c45bc37d4_s.jpg" alt="Xan and Fer's talk" /></a>
                            <a href="http://www.flickr.com/photos/gonzalemario/4840720086/" title="DSC_0021 by gonzalemario, on Flickr"><img src="http://farm5.static.flickr.com/4091/4840720086_0496ba4ff1_s.jpg" width="75" height="75" alt="DSC_0021" /></a>
                            <a href="http://www.flickr.com/photos/gonzalemario/4840105623/" title="DSC_0007 by gonzalemario, on Flickr"><img src="http://farm5.static.flickr.com/4109/4840105623_f62d4bc617_s.jpg" width="75" height="75" alt="DSC_0007" /></a>
                            <a href="http://www.flickr.com/photos/gonzalemario/4840729652/" title="DSC_0042 by gonzalemario, on Flickr"><img src="http://farm5.static.flickr.com/4147/4840729652_14c119e217_s.jpg" width="75" height="75" alt="DSC_0042" /></a>
                            <a href="http://www.flickr.com/photos/cmoi/4843165688/" title="IMG_8141 by pterjan, on Flickr"><img src="http://farm5.static.flickr.com/4132/4843165688_ab774b7d9e_s.jpg" width="75" height="75" alt="IMG_8141" /></a>
                            <a href="http://www.flickr.com/photos/cmoi/4843231248/" title="IMG_8174 by pterjan, on Flickr"><img src="http://farm5.static.flickr.com/4107/4843231248_eff013bac0_s.jpg" width="75" height="75" alt="IMG_8174" /></a>
                            <a href="http://www.flickr.com/photos/cmoi/4843672874/" title="Collabora Party by pterjan, on Flickr"><img src="http://farm5.static.flickr.com/4110/4843672874_398de8ec99_s.jpg" width="75" height="75" alt="Collabora Party" /></a>
                            <a href="http://www.flickr.com/photos/mariosp/4839872351/" title="Xan and Fer's talk by mariosp, on Flickr"><img src="http://farm5.static.flickr.com/4124/4839872351_0d81627f3d_s.jpg" width="75" height="75" alt="Xan and Fer's talk" /></a>
                            <a href="http://www.flickr.com/photos/gonzalemario/4840731690/" title="DSC_0052 by gonzalemario, on Flickr"><img src="http://farm5.static.flickr.com/4125/4840731690_99e27a1ae2_s.jpg" width="75" height="75" alt="DSC_0052" /></a>
                            <a href="http://www.flickr.com/photos/mdkflickr/4843336015/" title="2010-07-26 15.13.17 by MdKflickr, on Flickr"><img src="http://farm5.static.flickr.com/4105/4843336015_c312b00892_s.jpg" width="75" height="75" alt="2010-07-26 15.13.17" /></a>
                            <a href="http://www.flickr.com/photos/mdkflickr/4843338339/" title="2010-07-29 10.19.05 by MdKflickr, on Flickr"><img src="http://farm5.static.flickr.com/4128/4843338339_20d081a0d3_s.jpg" width="75" height="75" alt="2010-07-29 10.19.05" /></a>
                            <a href="http://www.flickr.com/photos/cmoi/4846661648/" title="IMG_8211 by pterjan, on Flickr"><img src="http://farm5.static.flickr.com/4148/4846661648_f708969cc8_s.jpg" width="75" height="75" alt="IMG_8211" /></a>
                            <a href="http://www.flickr.com/photos/overdreamss/4837974528/" title="Arquitectura Tipica II by Overdreamss, on Flickr"><img src="http://farm5.static.flickr.com/4105/4837974528_789a194420_s.jpg" width="75" height="75" alt="Arquitectura Tipica II" /></a>
                            <a href="http://www.flickr.com/photos/cmoi/4843601178/" title="Collabora Party by pterjan, on Flickr"><img src="http://farm5.static.flickr.com/4110/4843601178_35808a6eee_s.jpg" width="75" height="75" alt="Collabora Party" /></a>
                            <a href="http://www.flickr.com/photos/mdkflickr/4843955886/" title="2010-07-29 16.47.14 by MdKflickr, on Flickr"><img src="http://farm5.static.flickr.com/4083/4843955886_c93bb6c9a6_s.jpg" width="75" height="75" alt="2010-07-29 16.47.14" /></a>
                            <a href="http://www.flickr.com/photos/gonzalemario/4840730070/" title="DSC_0043 by gonzalemario, on Flickr"><img src="http://farm5.static.flickr.com/4087/4840730070_1b33f2549a_s.jpg" width="75" height="75" alt="DSC_0043" /></a>
                            <a href="http://www.flickr.com/photos/gonzalemario/4840732156/" title="DSC_0053 by gonzalemario, on Flickr"><img src="http://farm5.static.flickr.com/4090/4840732156_23a6c08ae2_s.jpg" width="75" height="75" alt="DSC_0053" /></a>
                            <a href="http://www.flickr.com/photos/cmoi/4842549135/" title="IMG_8145 by pterjan, on Flickr"><img src="http://farm5.static.flickr.com/4089/4842549135_ac81a93951_s.jpg" width="75" height="75" alt="IMG_8145" /></a>
                            <a href="http://www.flickr.com/photos/cmoi/4842574257/" title="IMG_8165 by pterjan, on Flickr"><img src="http://farm5.static.flickr.com/4092/4842574257_11f7859eb9_s.jpg" width="75" height="75" alt="IMG_8165" /></a>
                            <a href="http://www.flickr.com/photos/cmoi/4842576015/" title="IMG_8168 by pterjan, on Flickr"><img src="http://farm5.static.flickr.com/4148/4842576015_2b171fa055_s.jpg" width="75" height="75" alt="IMG_8168" /></a>
                            <a href="http://www.flickr.com/photos/gonzalemario/4840108627/" title="DSC_0020 by gonzalemario, on Flickr"><img src="http://farm5.static.flickr.com/4147/4840108627_8926743d44_s.jpg" width="75" height="75" alt="DSC_0020" /></a>
                            <a href="http://www.flickr.com/photos/gonzalemario/4840110669/" title="DSC_0032 by gonzalemario, on Flickr"><img src="http://farm5.static.flickr.com/4107/4840110669_8584f25190_s.jpg" width="75" height="75" alt="DSC_0032" /></a>
                            <a href="http://www.flickr.com/photos/gonzalemario/4840109291/" title="DSC_0022 by gonzalemario, on Flickr"><img src="http://farm5.static.flickr.com/4124/4840109291_6bc071a9de_s.jpg" width="75" height="75" alt="DSC_0022" /></a>
                            <a href="http://www.flickr.com/photos/mdkflickr/4843334953/" title="2010-07-26 15.10.29 by MdKflickr, on Flickr"><img src="http://farm5.static.flickr.com/4154/4843334953_1a4b60824e_s.jpg" width="75" height="75" alt="2010-07-26 15.10.29" /></a>
                            <a href="http://www.flickr.com/photos/mariosp/4840484462/" title="Xan and Fer's talk by mariosp, on Flickr"><img src="http://farm5.static.flickr.com/4111/4840484462_9c45bc37d4_s.jpg" width="75" height="75" alt="Xan and Fer's talk" /></a>
                            <a href="http://www.flickr.com/photos/mariosp/4840482582/" title="Igalia banner by mariosp, on Flickr"><img src="http://farm5.static.flickr.com/4087/4840482582_52a20c7c1e_s.jpg" width="75" height="75" alt="Igalia banner" /></a>
                            <a href="http://www.flickr.com/photos/mariosp/4840463600/" title="Waterloo statue by mariosp, on Flickr"><img src="http://farm5.static.flickr.com/4107/4840463600_f8a47ccb22_s.jpg" width="75" height="75" alt="Waterloo statue" /></a>
                        </div>
                        <p>
                            <em><a href="http://www.flickr.com/photos/tags/guadec/" rel="external" title="Photos on Flickr with the tag 'guadec'">View more photos of GUADEC 2010.</a></em>
                        </p>
                        
                    <h1>About GNOME</h1>
                    <p><img src="{$baseUrl}/img/gnome-logo.png" alt="The GNOME foot logo" style="float: left; margin: 5px 20px 0 0;" /><a href="http://www.gnome.org">GNOME</a> is an open source project that provides an attractive, usable and accessible computer interface environment for million of users for free. But more than anything else, GNOME is a worldwide community of volunteers who hack, translate, design, QA, and generally have fun together.
                    </p>
                            
                </div>
                
                <div class="clear"></div>
                
                <div class="grid_6 alpha">
                    <h2>The GNOME Open Desktop Day</h2>
                    <p>Prior to <abbr title="GNOME Users&#146; And Developers&#146; European Conference">GUADEC</abbr>, the GNOME Open Desktop Day will take place, which is in fact a preconference to GUADEC.</p>
                    <p>The GNOME Open Desktop Day will be organized in association with <a href="https://noiv.nl/service/english/" rel="external" title="Nederland Open in Verbinding  &raquo; English">Programme Office NOiV</a> &mdash; 'the Netherlands in Open Connection'.</p>

                    <p><a href="{url conference="guadec" schedConf="2010" page="schedConf" op="opendesktopday"}" title="The GNOME Open Desktop Day"><em>More information...</em></a></p>
                </div>
                
                <div class="grid_5">
                    <h2>GNOME Developer training</h2>
                    <p>On 26 July and 27 July 2010, the first GNOME Developer Training sessions will be run as part of <abbr title="GNOME Users&#146; And Developers&#146; European Conference">GUADEC</abbr> 2010.</p>
                    <p>The participants will be trained in developing on and for the GNOME desktop, and in working effectively with the community.</p>

                    <p><a href="{url conference="guadec" schedConf="2010" page="schedConf" op="training"}" title="GUADEC 2010 GNOME Developer training"><em>More information...</em></a></p>
                </div>
                
                <div class="grid_5 omega">
                    <h2 class="flickr">previous GNOME conferences on <span style="color: #3465a4;">Flick</span><span style="color: #ff048c;">r</span></h2>
                        
                        <div class="flickrPhotos">
                            <a href="http://www.flickr.com/photos/gpoo/3188692723/" title="“El panel de amor y odio a Gnome” by gpoo"><img src="http://farm4.static.flickr.com/3303/3188692723_5bb033a351_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/mariosp/4194972082/" title="“WebKitGTK+ 2009 Hackfest” by mariosp"><img src="http://farm5.static.flickr.com/4008/4194972082_d51c8653e3_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/lucbyhet/3687485143/" title="“GCDS Opening Keynote” by lucbyhet"><img src="http://farm3.static.flickr.com/2617/3687485143_8e0a752c3e_s.jpg" alt="" /></a>
                            <a href="http://www.flickr.com/photos/mariosp/3701083442/" title="“Joint Party on Sunday” by mariosp"><img src="http://farm4.static.flickr.com/3514/3701083442_4612162d85_s.jpg" alt="" /></a>
                            {*<a href="http://www.flickr.com/photos/gpoo/3335859855/" title="“KDE & GNOME” by gpoo"><img src="http://farm4.static.flickr.com/3542/3335859855_07eefa2615_s.jpg" alt="" /></a>*}
                            {*<a href="http://www.flickr.com/photos/penguincakes/2661153010/" title="“icecream deathmatch” by penguincakes"><img src="http://farm4.static.flickr.com/3196/2661153010_bc6fd4bc87_s.jpg" alt="" /></a>*}
                            {*<a href="http://www.flickr.com/photos/gpoo/3188658895/" title="“Assistentes” by gpoo"><img src="http://farm4.static.flickr.com/3511/3188658895_bf8b259a84_s.jpg" alt="" /></a>*}
                            <a href="http://www.flickr.com/photos/mariosp/3701067138/" title="“Exiting Alfredo Kraus auditorium” by mariosp"><img src="http://farm3.static.flickr.com/2604/3701067138_a44dab53ff_s.jpg" alt="" /></a>
                            {*<a href="http://www.flickr.com/photos/csaavedra/1177718793/" title="“The feet of the gnomes” by csaavedra"><img src="http://farm2.static.flickr.com/1399/1177718793_a8cbad0934_s.jpg" alt="" /></a>*}
                            <a href="http://www.flickr.com/photos/gpoo/3188694885/" title="“GNOME” by gpoo"><img src="http://farm4.static.flickr.com/3533/3188694885_fbd048c3d5_s.jpg" alt="" /></a>
                            {*<a href="http://www.flickr.com/photos/mariosp/3701081572/" title="“Sponsored by GNOME” by mariosp"><img src="http://farm4.static.flickr.com/3472/3701081572_40a83e429d_s.jpg" alt="" /></a>*}
                            {*<a href="http://www.flickr.com/photos/rossburton/2668456327/" title="“Advisory Board Meeting” by Ross Burton"><img src="http://farm4.static.flickr.com/3061/2668456327_18278926e5_s.jpg" alt="" /></a>*}
                        </div>
	            </div>
                
                <div class="clear"></div>
                
                
                {*
                
                Temporarily hiding four information blocks
                
                
                <div class="grid_16 omega">
                    <hr />
                </div>
                
                
                
                <!-- Four information blocks -->
                
                
                <div class="grid_4 alpha">
                    <h2>Location</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna metus, tempor eget dapibus non, viverra in dui. Etiam ullamcorper blandit dui, a scelerisque nibh tempus at. Praesent ultricies, nisl et facilisis mattis.</p>
                </div>
                
                <div class="grid_4">
                    <h2>Travel+Sleep+Eat</h2>
                    <p>Donec viverra lacinia pharetra. Sed nulla dui, consequat sagittis rutrum in, mollis id justo. Aenean ut metus sem. Suspendisse sed metus nisl, ac condimentum quam. Suspendisse varius congue urna, sed tempus arcu tempus at.</p>
                </div>
                
                <div class="grid_4">
                    <h2>Get involved</h2>
                    <p>
                        The organisation team calls you to arms! A community conference like GUADEC only happens when the community puts its weight behind it. This is your chance to be part of this event. Whether you are a conference rookie or a seasoned GUADEC veteran, your help is much appreciated. <a href="#">More »</a>
                    </p>
                </div>
                
                <div class="grid_4 omega">
                    <h2>The GUADEC Wiki</h2>
                    <p>Pellentesque pellentesque blandit eros ut luctus. Donec adipiscing rhoncus dolor vulputate rutrum. In diam libero, molestie sed egestas sit amet, vulputate malesuada massa. Suspendisse sit amet semper risus.</p>
                </div>
            
                <div class="clear"></div>
                
                *}
                
                <div class="grid_16 alpha omega">
                    <hr />
                </div>



{if $enableAnnouncementsHomepage}
	{* Display announcements *}
	<div class="grid_12 alpha">
		{*<h3>{translate key="announcement.announcementsHome"}</h3>*}
        <div class="headline">
            Latest News
        </div>
		{include file="announcement/list.tpl"}	
		<p><a href="{url page="announcement"}">{translate key="announcement.moreAnnouncements"}</a></p>
	</div>
	
    <div class="grid_4 omega">
        <h2><a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors"}" title="Sponsors">Sponsors</a></h3>
        <div style="background-color: #FFF; -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px; -moz-box-shadow: inset 0 1px 5px #999; -webkit-box-shadow: 0 1px 5px #999; box-shadow: inset 0 1px 5px #999; margin-top: 20px; text-align: center; padding: 20px; line-height: 150%;">
            {* Silver sponsors *}
            <h4>Silver sponsors</h4>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="canonical"}" title="Canonical"><img src="{$baseUrl}/img/spnsrs/canonical.png" alt="Canonical"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="collabora"}" title="Collabora"><img src="{$baseUrl}/img/spnsrs/collabora.png" alt="Collabora"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="google"}" title="Google"><img src="{$baseUrl}/img/spnsrs/google.png" alt="Google"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="igalia"}" title="Igalia"><img src="{$baseUrl}/img/spnsrs/igalia.png" alt="Igalia"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="intel"}" title="Intel"><img src="{$baseUrl}/img/spnsrs/intel.png" alt="Intel"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="lanedo"}" title="Lanedo"><img src="{$baseUrl}/img/spnsrs/lanedo.png" alt="Lanedo"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="linux-foundation"}" title="The Linux Foundation"><img src="{$baseUrl}/img/spnsrs/linux-foundation.png" alt="The Linux Foundation"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="meego"}" title="MeeGo"><img src="{$baseUrl}/img/spnsrs/meego.png" alt="MeeGo"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="nokia"}" title="Nokia"><img src="{$baseUrl}/img/spnsrs/nokia.png" alt="Nokia"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="opensuse"}" title="openSUSE.org"><img src="{$baseUrl}/img/spnsrs/opensuse.png" alt="openSUSE"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="redhat"}" title="RedHat"><img src="{$baseUrl}/img/spnsrs/redhat.png" alt="RedHat"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="ubuntu-user"}" title="Ubuntu User"><img src="{$baseUrl}/img/spnsrs/ubuntu-user.png" alt="Ubuntu User"/></a>
            </p>
            {* Bronze sponsors *}
            <h4>Bronze sponsors</h4>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="arm"}" title="ARM"><img src="{$baseUrl}/img/spnsrs/arm.png" alt="ARM"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="hxx"}" title="Hxx Foundation"><img src="{$baseUrl}/img/spnsrs/hxx.png" alt="Hxx"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="mozilla"}" title="Mozilla.org"><img src="{$baseUrl}/img/spnsrs/mozilla.png" alt="Mozilla"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="openismus"}" title="Openismus"><img src="{$baseUrl}/img/spnsrs/openismus.png" alt="Openismus"/></a>
            </p>
            {* Dutch sponsors *}
            <h4>National sponsors</h4>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="nlnet"}" title="NLnet"><img src="{$baseUrl}/img/spnsrs/nlnet.png" alt="NLnet Foundation"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="ictivity"}" title="Ictivity"><img src="{$baseUrl}/img/spnsrs/ictivity.png" alt="Ictivity"/></a>
            </p>
            {* Video streaming sponsors *}
            <h4>Video streaming</h4>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="fluendo"}" title="Fluendo"><img src="{$baseUrl}/img/spnsrs/fluendo.png" alt="Fluendo"/></a>
            </p>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="flumotion"}" title="Flumotion"><img src="{$baseUrl}/img/spnsrs/flumotion.png" alt="Flumotion"/></a>
            </p>
            {* Media partner *}
            <h4>Media partner</h4>
            <p>
            <a href="{url conference="guadec" schedConf="2010" page="schedConf" op="sponsors" anchor="linux-magazine"}" title="Linux Magazine"><img src="{$baseUrl}/img/spnsrs/linux-magazine.png" alt="Linux Magazine"/></a>
            </p>
            {*Does your company want to sponsor GUADEC 2010? Look for information and an application form in the <a href="http://guadec.org/GUADEC 2010-final.pdf" title="GUADEC 2010 Sponsor's Brochure">Sponsor's Brochure</a>.*}
        </div>
    </div>
	
	<div class="clear"></div>
{/if}

{if $homepageImage}
<div id="homepageImage"><img src="{$publicFilesDir}/{$homepageImage.uploadName|escape}" width="{$homepageImage.width}" height="{$homepageImage.height}" {if $homepageImage.altText != ''}alt="{$homepageImage.altText|escape}"{else}alt="{translate key="common.conferenceHomepageImage.altText"}"{/if} /></div>
{/if}


{$additionalHomeContent}

{* Display current scheduled conferences. *}
{*
<h3>{translate key="conference.currentConferences"}</h3>
{if not $currentSchedConfs->eof()}
	{iterate from=currentSchedConfs item=schedConf}
		<h4><a href="{url schedConf=$schedConf->getPath()}">{$schedConf->getFullTitle()|escape}</a></h4>
		<p>
			{$schedConf->getSetting('locationName')}<br/>
			{$schedConf->getSetting('locationAddress')|nl2br}<br/>
			{if $schedConf->getSetting('locationCity')}{$schedConf->getSetting('locationCity')|escape}{assign var="needsComma" value=true}{/if}{if $schedConf->getSetting('locationCountry')}{if $needsComma}, {/if}{$schedConf->getSetting('locationCountry')|escape}{/if}
		</p>
		<p>{$schedConf->getSetting('startDate')|date_format:$dateFormatLong} &ndash; {$schedConf->getSetting('endDate')|date_format:$dateFormatLong}</p>
		{if $schedConf->getLocalizedSetting('introduction')}
			<p>{$schedConf->getLocalizedSetting('introduction')|nl2br}</p>
		{/if}
		<p><a href="{url schedConf=$schedConf->getPath()}" class="action">{translate key="site.schedConfView"}</a>
	{/iterate}
{else}
	{translate key="conference.noCurrentConferences"}
{/if}
*}

{include file="common/footer.tpl"}
