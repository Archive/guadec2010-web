{**
 * index.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Display list of announcements. 
 *
 * $Id: index.tpl,v 1.9.2.1 2009/04/08 20:45:51 asmecher Exp $
 *}
{assign var="pageTitle" value="announcement.announcements"}
{assign var="pageId" value="announcement.announcements"}
{include file="common/header.tpl"}


<div class="grid_12 alpha">

{if $announcementsIntroduction != null}
	<p>{$announcementsIntroduction|nl2br}</p>
{/if}

{iterate from=announcements item=announcement}

    <div class="news">
        <div class="newsDate">
            <span class="month">{$announcement->getDatePosted('F')}</span>
            <span class="day">{$announcement->getDatePosted('jS')}</span>
            <span class="year">{$announcement->getDatePosted('Y')}</span>
        </div>
    	{if $announcement->getTypeId() != null}
    	    <h1><a href="{url page="announcement" op="view" path=$announcement->getAnnouncementId()}">{$announcement->getAnnouncementTypeName()|escape}: {$announcement->getAnnouncementTitle()|escape}</a></h1>
	    {else}
	        <h1><a href="{url page="announcement" op="view" path=$announcement->getAnnouncementId()}">{$announcement->getAnnouncementTitle()|escape}</a></h1>
	    {/if}
	    
	    {$announcement->getAnnouncementDescriptionShort()|nl2br}
	    
	    <p><a href="{url page="announcement" op="view" path=$announcement->getAnnouncementId()}">{translate key="announcement.viewLink"}</a></p>
    </div>
    
    
    {*
	<tr class="title">
	{if $announcement->getTypeId() != null}
		<td class="title"><h4>{$announcement->getAnnouncementTypeName()|escape}: {$announcement->getAnnouncementTitle()|escape}</h4></td>
	{else}
		<td class="title"><h4>{$announcement->getAnnouncementTitle()|escape}</h4></td>
	{/if}
		<td class="more">&nbsp;</td>
	</tr>
	<tr class="description">
		<td class="description">{$announcement->getAnnouncementDescriptionShort()|nl2br}</td>
		<td class="more">&nbsp;</td>
	</tr>
	<tr class="details">
		<td class="posted">{translate key="announcement.posted"}: {$announcement->getDatePosted()}</td>
		<td class="more"><a href="{url op="view" path=$announcement->getAnnouncementId()}">{translate key="announcement.viewLink"}</a></td>
	</tr>
	<tr>
		<td colspan="2" class="{if $announcements->eof()}end{/if}separator">&nbsp;</td>
	</tr>
	*}
{/iterate}
{if $announcements->wasEmpty()}
    {*
	<tr>
		<td colspan="2" class="nodata">{translate key="announcement.noneExist"}</td>
	</tr>
	<tr>
		<td colspan="2" class="endseparator">&nbsp;</td>
	</tr>
	*}
{else}
    {*
	<tr>
		<td align="left">{page_info iterator=$announcements}</td>
		<td align="right">{page_links anchor="announcements" name="announcements" iterator=$announcements}</td>
	</tr>
	*}
{/if}

</div>

{include file="common/footer.tpl"}
