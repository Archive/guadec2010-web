{**
 * view.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v1. For full terms see the file docs/COPYING.
 *
 * View full announcement text. 
 *
 * $Id: view.tpl,v 1.5.2.1 2009/04/08 20:45:51 asmecher Exp $
 *}
{assign var="pageTitleTranslated" value=$announcementTitle}
{assign var="pageId" value="announcement.view"}
{include file="common/header.tpl"}

<div class="grid_12 alpha suffix_4 omega">
    <div class="news">
        <div class="newsDate" style="margin-top: 7px;">
            <span class="month">{$announcement->getDatePosted('F')}</span>
            <span class="day">{$announcement->getDatePosted('jS')}</span>
            <span class="year">{$announcement->getDatePosted('Y')}</span>
        </div>
        {$announcement->getAnnouncementDescription()|nl2br}
    </div>
</div>

{include file="common/footer.tpl"}
