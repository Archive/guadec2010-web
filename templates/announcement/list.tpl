{**
 * list.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Display list of announcements without site header or footer. 
 *
 * $Id: list.tpl,v 1.7.2.1 2009/04/08 20:45:51 asmecher Exp $
 *}

{iterate from=announcements item=announcement}
    <div class="news">
        <div class="newsDate">
            <span class="month">{$announcement->getDatePosted('F')}</span>
            <span class="day">{$announcement->getDatePosted('jS')}</span>
            <span class="year">{$announcement->getDatePosted('Y')}</span>
        </div>
    	{if $announcement->getTypeId() != null}
    	    <h1><a href="{url page="announcement" op="view" path=$announcement->getAnnouncementId()}">{$announcement->getAnnouncementTypeName()|escape}: {$announcement->getAnnouncementTitle()|escape}</a></h1>
	    {else}
	        <h1><a href="{url page="announcement" op="view" path=$announcement->getAnnouncementId()}">{$announcement->getAnnouncementTitle()|escape}</a></h1>
	    {/if}
	    
	    {$announcement->getAnnouncementDescriptionShort()|nl2br}
	    
	    <p><a href="{url page="announcement" op="view" path=$announcement->getAnnouncementId()}">{translate key="announcement.viewLink"}</a></p>
    </div>
{/iterate}
{if $announcements->wasEmpty()}
{/if}
