{**
 * opendesktopday.tpl
 *
 * Copyright (c) 2010 Sanne te Meerman <sanne@opensourceadvies.nl>
 * Copyright (c) 2010 Sense Hofstede <qense@ubuntu.com>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * The GNOME Open Desktop Day 2010 information page.
 *
 *}
{assign var="pageTitleTranslated" value="The GNOME Open Desktop Day"}
{include file="common/header.tpl"}
{assign var="helpTopicId" value="user.conferenceInformation"}
                <div class="grid_11 alpha">
                    
<h2>GUADEC pre-conference: July 26</h2>
<p>
                    With the upcoming release of <a href="http://live.gnome.org/ThreePointZero">GNOME 3.0</a>, software development interests will focus increasingly on improved user experience, accessibility and application use. While the GUADEC 2010 conference aims to promote all of these ideals through the work of the GNOME Foundation, the GNOME Open Desktop Day will place important attention on the desktop in government and education.
                    </p>
                    <p>
                    The GNOME Open Desktop Day is a preconference to <abbr title="GNOME Users&#146; And Developers&#146; European Conference">GUADEC</abbr> 2010. It is organized in association with <a href="https://noiv.nl/service/english/" rel="external" title="Nederland Open in Verbinding  &raquo; English">Programme Office NOiV</a> &mdash; 'the Netherlands in Open Connection' &mdash; and <a href="https://wiki.noiv.nl/xwiki/bin/view/OpenDWR/">Open DWR</a>. In line with <a href="https://noiv.nl/files/2009/12/Action_plan_english.pdf" rel="external" title="NOiV action plan (English)">the NOiV's action plan</a>, the preconference will focus on the 'New Forerunners' <em>(action line 9, page 19 of the action plan)</em> and other organisations in the public/private sectors interested in open desktop development. <p>
Interesting open desktop software 'early adopter' projects will share their experiences collaborating with governmental institutions, software communities and businesses. In presentations and interactive sessions, there's room for technical details as well as discussion of backgrounds and strategies. The program schedule is below. </p>
<p>
Registration is at <a href="https://register.guadec.org/">register.guadec.org</a>. For more information, or if you are an open-software business interested in participating, please contact Sanne te Meerman at &mdash; <em>sanne &lt;at sign&gt; opensourceadvies.nl</em> &mdash or at +31 6-1482-9390.</p>
<p>&nbsp;</p><p>
<a href="https://register.guadec.org/"><img src="http://www.guadec.org/img/register_now.png"></a>
                    </p>

                </div>
                
                <div class="grid_5 omega">
                    <a name="sponsors"></a><h2>Sponsors</h2>
                    <p> The GNOME Open Desktop Day is supported by:</p>

                    <p>
                    <a lang="en-GB" href="https://noiv.nl/service/english/" rel="external" title="Nederland Open in Verbinding  &raquo; English"><img src="{$baseUrl}/img/tgood-spnsrs/noiv.png" alt="NOiV" /></a>
                    <a lang="en-GB" href="http://www.minbzk.nl/english/" rel="external" title="(Dutch) Ministry of the Interior and Kingdom Relations"><img src="{$baseUrl}/img/tgood-spnsrs/min-biza.png" alt="Ministerie van Binnenlandse Zaken en Koninkrijksrelaties" /></a>
                    <a lang=nl-NL" href="http://www.ictivity.nl/" rel="external" title="Ictivity"><img src="{$baseUrl}/img/tgood-spnsrs/ictivity.png" alt="Ictivity" /></a>
                    <a href="http://lpi.org/" rel="external" title="Linux Professional Institute"><img src="{$baseUrl}/img/tgood-spnsrs/lpi.png" alt="LPI" /></a>
                    </p>

                    <p>
                    The <a href="#arjen-kamphuis" title="Arjen Kamphuis">Chair of the Day</a> is provided by:<br/>
                    <a lang="nl-NL" href="http://www.gendo.nl/" rel="external" title="Gendo"><img src="{$baseUrl}/img/tgood-spnsrs/gendo.png" title="Gendo" alt="Gendo" /></a>
                    </p>

                </div>
                
                <div class="grid_16 alpha omega">
                    <a name="schedule"></a>
                    <h2>Schedule</h2>
                    <div id="opendesktopschedule">
                    <table>
                    <thead>
                    <tr>
                        <th>Time</th>
                        <th>Event</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>9:30 - 11:00</td>
                        <td>
                            <font color="gray"><strong>Information market</strong><br/><br/>
                            <font color="black"><em>CANCELLED</em></font>
                        </td>
                    </tr>
                    <tr>
                        <td>11:00 - 12:00</td>
                        <td>
                            <strong>Meet the GNOME Foundation</strong><br/><br/>
                            The GNOME project started in 1997. Since then, GNOME has become the default desktop environment in many successful Linux, BSD and Unix distributions. In this session, <a href="#stormy-peters" title="Stormy Peters"><em>Stormy Peters</em></a>, Executive Director of the GNOME Foundation, and other members of the GNOME organization will present some of GNOME's rich history, take a look at past succeses, give the audience insights on GNOME's path forward in the future.
                        </td>
                    </tr>
                    <tr>
                        <td>12:00</td>
                        <td class="purple">Lunch</td>
                    </tr>
                    <tr>
                        <td>13:00 - 14:00</td>
                        <td>
                            <strong>Big deployments in Europe: a system integrator's perspective</strong><br/><br/>
                        	<a href="#steve-george" title="Steve George"><em>Steve George</em></a> of <a href="http://www.canonical.com/">Canonical</a> talks about Canonical's experience with amongst others, the migration of 15,000 laptops in France to a Linux-based desktop. How were these projects handled from a strategic point of view? What challenges in terms of application integration were encountered? What did it mean in terms of cost-savings?</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>14:00</td>
                        <td class="purple">Break</td>
                    </tr>
                    <tr>
                        <td>14:15 - 15:45</td>
                        <td>
                            <strong>Ubiquitous Open Source in Spain: How government, business and community work together</strong><br/><br/>
                        	Andalucia, a Spanish region with 8 million inhabitants, has launched a Linux distribution of called <a href="http://www.guadalinex.org/">GuadaLinex</a>. The system is now used on more than 500,000 desktops in public education, 764 community 'cybercenters', over 600 public libraries and some 200 elderly care centers. <em><a href="#juan-conde" title="Juan Conde">Juan Conde</a></em>, FOSS manager of the telecommunications and information society of Junta de Andaluc&iacute;a, and <em><a href="#jose-felix-ontanon" title="Jos&eacute; F&eacute;lix Onta&ntilde;&oacute;n">Jos&eacute; F&eacute;lix Onta&ntilde;&oacute;n</a></em>, project manager for Guadalinfo software, will guide the audience through an in-depth look at their work, from project and technical issues to software maintenance and how the project feeds other Linux distributions.
                        </td>
                    </tr>
                    <tr>
                        <td>15:45</td>
                        <td class="purple">Break</td>
                    </tr>
                    <tr>
                        <td>16:00 - 17:30</td>
                        <td>
                            <strong>How we can move forward in the Netherlands with free and open software on the desktop</strong> <em>(interactive session)</em><br/><br/>
                        	With:	
	                        <ul>
	                            <li><em><a href="#stephan-wildeboer" title="Stephan Wildeboer">Stephan Wildeboer</a></em>, project manager of several government projects regarding the use of free and open software in the Netherlands.</li>
	                            <li><em><a href="#arjen-kamphuis" title="Arjen Kamphuis">Arjen Kamphuis</a></em>, independent advisor and co-author of the first initiative in parliament to push free and open software in government.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>17:30</td>
                        <td class="purple">Drinks</td>
                    </tr>
                    </tbody>
                    </table>
                    </div>
                </div>
                
                <div class="grid_16 alpha omega">

                    <a name="speakers"></a>
                    <h2>Speakers</h2>

                    <div id="speakerstable">
                        <table>
                        <tbody>
                        <tr>
                            <td>
                                <a name="juan-conde"></a><img src="{$baseUrl}/img/tgood-speakers/juan_conde.jpg" alt="" /><br /> <strong>Juan Conde</strong>
                            </td>
                            <td>
                                <strong>Juan Conde</strong> is from Sevilla, Spain. He studied Chemistry, and recieved his PhD in 1985. After teaching university-level courses, he left academia in 1990 to work with the Regional Government of Andalus&iacute;a as an IT technician, doing analysis and design development as well as management consulting. Since 2001 he has been working on usage, development and promotion in the field of free open source software. He is a manager of the Free Software section for the secretariat of telecommunications and information society of the Junta de Andaluc&iacute;a.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a name="jose-felix-ontanon"></a><img src="{$baseUrl}/img/tgood-speakers/jose_felix_ontanon.jpg" alt="" /><br /> <strong>Jos&eacute; F&eacute;lix Onta&ntilde;&oacute;n</strong>
                            </td>
                            <td>
                                <strong>Jos&eacute; F&eacute;lix Onta&ntilde;&oacute;n</strong> has studied Computing Engineering in Seville and works as project manager for the <abbr title="free and open source software">FOSS</abbr> company 'Emergya'. He worked as core-developer for the Andalucian distribution <a lang="es-ES" href="http://www.guadalinex.org/" rel="external" title="Portal Guadalinex">Guadalinex</a>. He is a key coordinator in the Guadalinfo Accesibillity project, which aims to enhance and improve the Orca screen reader and OCRFeeder, a document layout analysis and optical character recognition system. Besides managing the current Guadalinfo software, he also helps run a project of 740 telecenters in Andalusian towns and municipalities with their own GNOME-based GNU/Linux flavour Guadalinex Guadalinfo.
                            </td>
                        </tr>

<tr>
                            <td>
                                <a name="steve george"></a><img src="{$baseUrl}/img/tgood-speakers/stevegeorge2010m.jpg" alt="" /><br /> <strong>Steve George</strong>
                            </td> 
                            <td>
                                <strong>Steve George</strong> Steve George is Vice President of Business Development at Canonical Limited, the sponsors of Ubuntu. He is responsible for leading strategic initiatives and relationships that broaden the take-up of the Ubuntu platform amongst developers and users. Since Steve joined Canonical in 2006 he has helped the company build its commercial open source strategy, with a focus on business development, product management and operations.
                            </td> 
                        </tr>
                        <tr>
                            <td>
                                <a name="arjen-kamphuis"></a>
                                <img src="{$baseUrl}/img/tgood-speakers/arjen_kamphuis.jpg" alt="" /><br/><strong>Arjen Kamphuis</strong><br/> <em>Chair of the Day</em>
                            </td>
                            <td>
                                <strong>Arjen Kamphuis</strong> is co-founder and <abbr title="Chief Technology Officer">CTO</abbr> of <a lang="nl-NL" href="http://www.gendo.nl/" rel="external" title="Gendo">Gendo</a>. He studied Science &amp; Policy at Utrecht University and worked for IBM and Twynstra Gudde as IT architect, trainer and IT strategy advisor. Since 2002 he has been involved in the development of public IT policy in the area of open standards and opensource for the government and public sector.<br/>
                                Arjen advises senior managers and administrators of companies and public institutions, members of parliament and the Dutch Cabinet on the opportunities offered by open standards and open source software for the European knowledge economy and the society as a whole. He co-authored the 2007 Dutch government policy which mandated the use of open source software for government and public sector IT-operations.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a name="stormy-peters"></a><img src="{$baseUrl}/img/tgood-speakers/stormy_peters.jpg" alt="" /><br/> <strong>Stormy Peters</strong> <br/> <em>Executive Director of the GNOME Foundation</em>
                            </td>
                            <td>
                                <strong>Stormy Peters</strong> is an IT industry analyst and a prominent <abbr title="free and open source software">FOSS</abbr> advocate, promoting business use of <abbr title="free and open source software">FOSS</abbr>. She completed a Bachelor of Arts with a major in Computer Science at Rice University and initially worked as a software engineer for Hewlett-Packard in their Unix development team, where she also founded the Hewlett-Packard Open Source Program office. In 2000 she became one of the founding members of the GNOME Foundation Advisory board. She became Executive Director of the GNOME Foundation in 2008.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a name="stephan-wildeboer"></a><img src="{$baseUrl}/img/tgood-speakers/stephan_wildeboer.png" alt="" /><br /> <strong>Stephan Wildeboer</strong>
                            </td>
                            <td>
                                <strong>Stephan Wildeboer</strong> is a management consultant and the senior consultant Open Source strategy at two different companies. He is currently active as a Program Manager of the Open Desktop project of the city of Amsterdam. He has also been Interim Program Manager of OSOSS, the precedessor of Program Office NOiV. NOiV is the Dutch government organisation for support and advise on open standards and open source adoption in the public and semi-public sector in the Netherlands.
                            </td>
                        </tr>
                        </tbody>
                        </table>
<p align="right"><a href="http://www.guadec.org/docs/GNOME_ Open_Desktop_Day.pdf">Click here to see the original brochure (<em>Dutch</em>).</a> <a href="http://www.guadec.org/docs/GNOME_ Open_Desktop_Day.pdf"><img src="http://www.guadec.org/img/brochure-smallpic.png"></a></p>
                    </div>
                </div>

{include file="common/footer.tpl"}
