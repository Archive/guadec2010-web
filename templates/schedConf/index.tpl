{**
 * index.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Scheduled conference index page. Displayed when both a conference and a
 * scheduled conference have been specified.
 *
 * $Id: index.tpl,v 1.28.2.2 2009/04/08 20:45:52 asmecher Exp $
 *}
{*
 * The page and crumb titles differ here since the breadcrumbs already include
 * the conference title, but the page title doesn't.
 *}
{assign var="pageCrumbTitleTranslated" value=$schedConf->getSchedConfTitle()}
{assign var="pageTitleTranslated" value=$schedConf->getFullTitle()}
{include file="common/header.tpl"}


<div class="grid_12 alpha">

    <p class="feature">GUADEC highlights the capabilities and direction of GNOME – the user environment for desktop computers, networked servers and portable Internet devices. GUADEC also features discussions of the future of Free Software and Open Source development in general.</p>

    <h2>The History of GUADEC</h2>

    <p>This is the eleventh year in which the developers and users of GNOME will gather in person to discuss the future direction of the project, and to spread word of GNOME and the benefits of Free Software.</p>

    <p>The story of GUADEC begins in Paris in 1998, where a number of GNOME developers, who had worked together via the internet, thought it might be nice to finally meet in real life. They have met at the GUADEC conference every year since then.</p>
    
    <div class="grid_7 alpha">
        <img src="{$baseUrl}/img/about-map.png" alt="" />
    </div>
    
    <div class="grid_5 omega">
        <p>Every year, at different cities of World GUADEC brings together more than 500 people, with a dazzling array of keynote speakers from disparate sectors such as government and education, as well as GNOME community leaders. We were also pleased to welcome several key KDE and FreeDesktop.org developers, resulting in some important advances in desktop interoperability for GNU/Linux and Unix.</p>
        <p>By years between 2000 and 2009, GUADEC was in Paris (France), Copenhagen (Denmark), Seville (Spain), Dublin (Ireland), Kristiansand (Norway), Stuttgart (Germany), Vilanova (Catalonia), Birmingham (England), Istanbul (Turkey), Gran Canaria (Spain). And upcoming conference, GUADEC 2010, will take place in The Hague (Netherlands).</p>
    </div>
    
    <div class="clear"></div>
    
</div>

<div class="grid_4 omega">
    
    {if $schedConfPostOverview || $schedConfShowCFP || $schedConfShowSubmissionLink 
			    || $schedConfPostPolicies || $schedConfShowProgram ||  $schedConfPostPresentations || $schedConfPostSchedule 
			    || $schedConfPostPayment  || $schedConfPostAccommodation || $schedConfPostSupporters  || $schedConfPostTimeline}
    <div class="headline">Important information</div>

    <ul class="navigationList">
	    <li><a href="{url page="streams"}" title="Video Live Streams">Video Live Streams</a></li>
	    {if $schedConfPostOverview}<li><a href="{url page="schedConf" op="overview"}">Schedule overview</a></li>{/if}
	    {if $schedConfShowCFP}
		    <li><a href="{url page="schedConf" op="cfp"}">{translate key="schedConf.cfp"}</a>{if $submissionsOpenDate} ({$submissionsOpenDate|date_format:$dateFormatLong} - {$submissionsCloseDate|date_format:$dateFormatLong}){/if}</li>
	    {/if}
	    {if $schedConfShowSubmissionLink}
		    <li><a href="{url page="presenter" op="submit" requiresPresenter="1"}">{translate key="schedConf.proposalSubmission"}</a></li>
	    {/if}
	    {if $schedConfPostTrackPolicies}<li><a href="{url page="schedConf" op="trackPolicies"}">{translate key="schedConf.trackPolicies"}</a></li>{/if}
	    {if $schedConfShowProgram}<li><a href="{url page="schedConf" op="program"}">{translate key="schedConf.program"}</a></li>{/if}
	    {if $schedConfPostPresentations}<li><a href="{url page="schedConf" op="presentations"}">{translate key="schedConf.presentations.short"}</a></li>{/if}
	    {if $schedConfPostSchedule}<li><a href="{url page="schedConf" op="schedule"}">{translate key="schedConf.schedule"}</a></li>{/if}
	    {if $schedConfPostPayment}<li><a href="{url page="schedConf" op="registration"}">{translate key="schedConf.registration"}</a></li>{/if}
	    {if $schedConfPostAccommodation}<li><a href="{url page="schedConf" op="accommodation"}">{translate key="schedConf.accommodation"} and Venue</a></li>{/if}
	    {if $schedConfPostSupporters}<li><a href="{url page="about" op="organizingTeam"}">{translate key="schedConf.supporters"}</a></li>{/if}
	    {if $schedConfPostTimeline}<li><a href="{url page="schedConf" op="timeline"}">{translate key="schedConf.timeline"}</a></li>{/if}
	    <li><a href="{url page="schedConf" op="sponsors"}" title="Sponsors">Sponsors</a></li>
    </ul>
    {/if}
    
    <div class="headline">Other events during GUADEC</div>
    
    <ul class="navigationList">
        <li><a href="{url page="schedConf" op="opendesktopday"}">The GNOME Open Desktop Day</a></li>
	    <li><a href="{url page="schedConf" op="training"}">GUADEC 2010 GNOME developer training</a></li>
	    <li><a href="http://www.gnu.org/ghm/" rel="external">GNU Hackers’ Meeting</a></li>
    </ul>

    <div class="headline">Other pages of interest</div>

    <ul class="navigationList">
	    <li><a href="{url page="livestream"}" title="Conference livestream">Social media livestream</a></li>
	    <li><a rel="external" href="http://planet.gnome.org" title="Planet GNOME">Aggregated GNOME blogs</a></li>
    </ul>

</div>



{*<div>{$schedConf->getLocalizedSetting("introduction")|nl2br}</div>*}

{*
{if $enableAnnouncementsHomepage}
	<div id="announcementsHome">
		<h3>{translate key="announcement.announcementsHome"}</h3>
		{include file="announcement/list.tpl"}	
		<p><a href="{url page="announcement"}">{translate key="announcement.moreAnnouncements"}</a></p>
	</div>
{/if}
*}

{*
{if $homepageImage}
<div id="homepageImage"><img src="{$publicConferenceFilesDir}/{$homepageImage.uploadName|escape}" width="{$homepageImage.width}" height="{$homepageImage.height}" {if $homepageImage.altText != ''}alt="{$homepageImage.altText|escape}"{else}alt="{translate key="common.conferenceHomepageImage.altText"}"{/if} /></div>
{/if}
*}

{*
{$additionalHomeContent}
*}


{include file="common/footer.tpl"}
