{**
 * index.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Copyright (c) 2010 Sense Hofstede <qense@ubuntu.com>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Conference index page. Displayed when a conference, but not a scheduled
 * conference, has been selected.
 *
 * $Id: index.tpl,v 1.19.2.1 2009/04/08 20:45:51 asmecher Exp $
 *}
{assign var="pageTitleTranslated" value="GUADEC 2010 Sponsors"}
{include file="common/header.tpl"}

<div id="sponsors">
<p>
<abbr title="GNOME Users&#146; And Developers&#146; European Conference">GUADEC</abbr> 2010 wouldn't be possible with the generous support from our sponsors. This page provides an overview of most important sponsors and gives a description of them.
</p>

<div class="clear"></div>

{* Silver sponsors *}

<div class="grid_4 alpha">
<h4>Silver sponsors</h4>
</div>

<div class="grid_4">&nbsp;</div>
<div class="grid_4">&nbsp;</div>
<div class="grid_4 omega">&nbsp;</div>


<div class="clear"></div>

<div class="grid_4 alpha" itemscope itemtype="http://data-vocabulary.org/Organization">
<p class="heading">
<a lang="en-GB" href="http://www.canonical.com/" rel="external" itemprop="url" title="Canonical"><img src="{$baseUrl}/img/spnsrs-overview/canonical.png" alt="Canonical" id="canonical"/></a>
</p>

<p>
<strong>Canonical is organising the party in cafe 'Le Paris' on Wednesday.</strong>
</p>

<p>
As a member of the free software community, <span itemprop="name">Canonical</span> is committed to supporting the development of the open source ecosystem. Our employees are passionate about technology, open source and, of course, Ubuntu.</p>

<p>
While our main goal is to ensure that Ubuntu continues to be freely available to everyone around the world, we are also focused on building new tools and software to encourage innovation. Projects like Upstart, Apport, Launchpad and Bazaar are just a few examples of our ongoing commitment to initiatives that promote software development and collaboration throughout the community.
</p>

<p>
Learn more at <a lang="en-GB" href="http://www.canonical.com/" itemprop="url" rel="external" title="Canonical">www.canonical.com</a>.
</p>

</div>

<div class="grid_4" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a lang="en-GB"  href="http://www.collabora.co.uk/" itemprop="url" rel="external"  title="Collabora"><img src="{$baseUrl}/img/spnsrs-overview/collabora.png" alt="Collabora" id="collabora"/></a>
</p>

<p>
<strong>Collabora is organising the beach BBQ party on Thursday at Scheveningen.</strong>
</p>

<p>
<span itemprop="name">Collabora</span> is a multinational Open Source consultancy specializing in bringing companies and the Open Source software community together.</p>

<p>We give clients the knowledge, experience and infrastructure to allow them to become an integral part of the Open Source community. We provide a full range of services based around Open Source technologies including architecture, software development, project management, infrastructure and community development. We are skilled in a wide range of areas from consumer devices to multimedia and real-time communications systems. Collabora prides itself in creating a network of Open Source experts from all around the world.
</p>

<p>
Learn more at <a lang="en-GB"  href="http://www.collabora.co.uk/" itemprop="url" rel="external"  title="Collabora">www.collabora.co.uk</a>.
</p>

</div>

<div class="grid_4" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a href="http://www.google.com/" itemprop="url" rel="external" title="Google"><img src="{$baseUrl}/img/spnsrs-overview/google.png" alt="Google" id="google"/></a>
</p>

<p>
<span itemprop="name">Google</span>'s innovative search technologies connect millions of people around the world with information every day. Founded in 1998 by Stanford Ph.D. students Larry Page and Sergey Brin, Google today is a top web property in all major global markets. Google's targeted advertising program provides businesses of all sizes with measurable results, while enhancing the overall web experience for users.</p>

<p>
Google is headquartered in <span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address"><span itemprop="region">Silicon Valley</span></span> with offices throughout the Americas, Europe, Oceania and Asia.
</p>

<p>
Learn more at <a href="http://www.google.com/" itemprop="url" rel="external" title="Google">www.google.com</a>.
</p>

</div> 

<div class="grid_4 omega" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a lang="en-GB" href="http://www.igalia.com/" itemprop="url" rel="external" title="Igalia"><img src="{$baseUrl}/img/spnsrs-overview/igalia.png" alt="Igalia" id="igalia"/></a>
</p>
<p>
<span itemprop="name">Igalia</span> is an open source consultancy specialized in the development of
innovative free software technologies and solutions.
</p>

<p>
Igalia has been increasing its involvement in the GNOME community
since the creation of the company nine years ago, contributing with
code and documentation to various platform components and
applications, and sponsoring and organizing events. Igalia is a member
of the GNOME Foundation's Advisory Board.
</p>

<p>
Besides working in the desktop, the company applies its wide
experience on free software, GNOME, WebKit, GStreamer, and other Linux
and Freedesktop.org technologies to the mobile market, contributing to
different platforms and developing projects for relevant international
companies.
</p>

<p>
Learn more at <a lang="en-GB" href="http://www.igalia.com/" itemprop="url" rel="external" title="Igalia">www.igalia.com</a>.
</p>

</div>

<div class="clear"></div>

<div class="grid_4 alpha" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a href="http://www.intel.com/opensource" itemprop="url" rel="external" title="Intel"><img src="{$baseUrl}/img/spnsrs-overview/intel.png" alt="Intel" id="intel"/></a>
</p>
<p>

<p>
<span itemprop="name">Intel</span>, the world leader in silicon innovation, develops technologies, products and initiatives to continually advance how people work and live.
</p>

<p>
Intel Software provides technologies, products and services developers need to create innovative products and industry-leading software solutions on Intel platforms.
</p>

<p>
Intel has been actively innovating with the open source community and collaborates with commercial vendors to develop Linux and key Open Source solutions.
</p>

<p>Learn more at <a href="http://www.intel.com/opensource" itemprop="url" rel="external" title="Intel">http://www.intel.com/opensource</a>.</p>

</div>

<div class="grid_4" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a lang="en-GB" href="http://www.lanedo.com/" itemprop="url" rel="external" title="Lanedo"><img src="{$baseUrl}/img/spnsrs-overview/lanedo.png" alt="Lanedo" id="lanedo"/></a>
</p>
<p>
We believe in open source and we have a passion for it! The developers at <span itemprop="name">Lanedo</span> have delivered infrastructure to the GNOME platform for over a decade and been directly involved in the Maemo & MeeGo platforms. Providing leading expertise and services around the GNOME ecosystem, Linux and Unix in general.</p>

<p>Learn more at <a lang="en-GB" href="http://www.lanedo.com/" itemprop="url" rel="external" title="Lanedo">www.lanedo.com</a>.</p>

</div>

<div class="grid_4" itemscope itemtype="http://data-vocabulary.org/Organization">
<a name="linux-foundation"></a>

<p>
<a href="http://www.linuxfoundation.org/" rel="external" itemprop="url" title="The Linux Foundation"><img src="{$baseUrl}/img/spnsrs-overview/linux-foundation.png" alt="The Linux Foundation"/></a>
</p>

<p>
Founded in 2007, the <span itemprop="name">Linux Foundation</span> sponsors the work of Linux creator Linus Torvalds and is supported by leading Linux and open source companies and developers from around the world. The Linux Foundation promotes, protects and standardizes Linux by providing unified resources and services needed for open source to successfully compete with closed platforms.
</p>

<p>
Learn more at <a href="http://www.linuxfoundation.org/" rel="external" itemprop="url" title="The Linux Foundation">www.linuxfoundation.org</a>.
</p>

</div>

<div class="grid_4 omega" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a  href="http://meego.com/" rel="external" itemprop="url" title="MeeGo"><img src="{$baseUrl}/img/spnsrs-overview/meego.png" alt="MeeGo" id="meego"/></a>
</p>

<p>
<strong>MeeGo is sponsoring the refreshments and snacks during the main conference.</strong>
</p>

<p>
Learn more at <a  href="http://meego.com/" rel="external" itemprop="url" title="MeeGo">meego.com</a>.
</p>

</div>

<div class="clear"></div>

<div class="grid_4 alpha" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a  href="http://www.nokia.com/" rel="external" itemprop="url" title="Nokia"><img src="{$baseUrl}/img/spnsrs-overview/nokia.png" alt="Nokia" id="nokia"/></a>
</p>

<p>
Learn more at <a  href="http://www.nokia.com/" rel="external" itemprop="url" title="Nokia">www.nokia.com</a>.
</p>

</div>

<div class="grid_4" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a href="http://www.opensuse.org/en/" rel="external" itemprop="url" title="openSUSE.org"><img src="{$baseUrl}/img/spnsrs-overview/opensuse.png" alt="openSUSE" id="opensuse"/></a>
</p>

<p>
<span itemprop="name">The openSUSE Project</span> is a world-wide project sponsored by Novell that promotes the use of Linux everywhere.
</p>

<p>
The openSUSE Linux distribution provides everything you need to get started with Linux on the server, desktop, and for mobile computing.
</p>

<p>
The openSUSE Project is a world-wide community of contributors working together to create and distribute the world‘s most usable Linux distribution.
</p>

<p>
Learn more at <a href="http://www.opensuse.org/en/" rel="external" itemprop="url" title="openSUSE.org">www.opensuse.org</a>.
</p>

</div>

<div class="grid_4" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a href="http://www.redhat.com/" rel="external" itemprop="url" title="redhat.com"><img src="{$baseUrl}/img/spnsrs-overview/redhat.png" alt="RedHat" id="redhat"/></a>
</p>

<p>
<span itemprop="name">Red Hat</span>, the world's leading provider of open source solutions and an S&P 500 company, is headquartered in <span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address"><span itemprop="locality">Raleigh</span>, <span itemprop="region">NC</span></span> with over 65 offices spanning the globe. CIOs ranked Red Hat as one of the top vendors delivering value in Enterprise Software for six consecutive years in the CIO Insight Magazine Vendor Value survey.
</p>

<p>
Red Hat provides high-quality, affordable technology with its operating system platform, Red Hat Enterprise Linux, together with virtualization, applications, management and Services Oriented Architecture (SOA) solutions, including Red Hat Enterprise Virtualization and JBoss Enterprise Middleware.
</p>

<p>
Red Hat also offers support, training and consulting services to its customers worldwide.
</p>

<p>
Learn more at <a href="http://www.redhat.com/" rel="external" itemprop="url" title="redhat.com">www.redhat.com</a>.
</p>

</div>

<div class="grid_4 omega" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a href="http://www.ubuntu-user.com/" rel="external" itemprop="url" title="Ubuntu User (English)"><img src="{$baseUrl}/img/spnsrs-overview/ubuntu-user.png" alt="Ubuntu User" id="ubuntu-user"/></a>
</p>

<p>
<abbr title="GNOME Users&#146; And Developers&#146; European Conference">GUADEC</abbr> 2010 is sponsored by <span itemprop="name">Ubuntu User</span>
    <a href="http://www.ubuntu-user.com/" rel="external" itemprop="url" title="Ubuntu User (United States)">United States</a> and
    <a lang="de-DE" href="http://www.ubuntu-user.de/" rel="external" itemprop="url" title="Ubuntu User (Deutschland)">Germany</a>.
</p>

</div>

<div class="clear"></div>

{* Bronze sponsors *}

<div class="grid_4 alpha">
<h4>Bronze sponsors</h4>
</div>

<div class="grid_4">&nbsp;</div>
<div class="grid_4">&nbsp;</div>
<div class="grid_4 omega">&nbsp;</div>

<div class="clear"></div>

<div class="grid_4 alpha" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a href="http://www.arm.com/" rel="external" itemprop="url" title="ARM"><img src="{$baseUrl}/img/spnsrs-overview/arm.png" alt="ARM" id="arm"/></a></p>
<p>
<span itemprop="name">ARM</span> designs the technology that lies at the heart of advanced digital products, from wireless, networking and consumer entertainment solutions to imaging, automotive, security and storage devices. ARM's comprehensive product offering includes 32-bit RISC microprocessors, graphics processors, enabling software, cell libraries, embedded memories, high-speed connectivity products, peripherals and development tools.</p>

<p>
Combined with comprehensive design services, training, support and maintenance, and the company's broad Partner Community, they provide a total system solution that offers a fast, reliable path to market for leading electronics companies.
</p>

<p>
Learn more at <a href="http://www.arm.com/" itemprop="url" rel="external" title="ARM">www.arm.com</a>.
</p>

</div>

<div class="grid_4" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a href="http://hxxfoundation.nl/" rel="external" itemprop="url" title="Hxx Foundation"><img src="{$baseUrl}/img/spnsrs-overview/hxx.png" alt="Hxx" id="hxx"/></a>
</p>

<p>
The goal of the <span itemprop="name">Hxx Foundation</span>: organisation of congresses, festivals and lectures in the field of or relating to computertechnology and society, as well as anything that is directly or indirectly relating or conducive to aforementioned intentions, all in the broadest sense possible.
</p>

<p>
Learn more at <a href="http://hxxfoundation.nl/" rel="external" itemprop="url" title="Hxx Foundation">hxxfoundation.nl</a>.
</p>

</div> 

<div class="grid_4" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a  href="http://www.mozilla.org/" rel="external" itemprop="url" title="Mozilla.org"><img src="{$baseUrl}/img/spnsrs-overview/mozilla.png" alt="Mozilla" id="mozilla"/></a>
</p>
<p>
<span itemprop="name">Mozilla</span> is an international non-profit organization and a global community dedicated to improving the Internet experience for people everywhere. We create free, open source products and technologies through highly disciplined, transparent and cooperative process. Everything we create is a public asset available for others to use, adapt and improve. The result is great products built by passionate people &mdash; and better choices for everyone.
</p>

<p>
Learn more at <a  href="http://www.mozilla.org/" rel="external" title="Mozilla.org">www.mozilla.org</a>.
</p>

</div>

<div class="grid_4 omega" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a href="http://www.openismus.com/" rel="external" itemprop="url" title="Openismus"><img src="{$baseUrl}/img/spnsrs-overview/openismus.png" alt="Openismus" id="openismus"/></a>
</p>

<p>
<span itemprop="name">Openismus</span> is the <span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address"><span itemprop="country-name">German</span> open-source consultancy. We provide expertise in C and C++ on Linux, using GTK+ and Qt. Our senior developers in <span itemprop="locality">Berlin</span></span> are valued team members with reputations earned in open source projects.
</p>

<p>
We also train employees in-house, creating new developers who share our tradition of quality, focus, and clear communication.
</p>

<p>
Learn more at <a href="http://www.openismus.com/" rel="external" itemprop="url" title="Openismus">www.openismus.com</a>.
</p>

</div> 

<div class="clear"></div>

<div class="grid_4 alpha"><h4>National sponsors</h4></div>
<div class="grid_4">&nbsp;</div>
<div class="grid_4"><h4>Silver sponsors</h4></div>
<div class="grid_4 omega">&nbsp;</div>

<div class="clear"></div>

<div class="grid_4 alpha" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a lang="en-GB" href="http://www.nlnet.nl/" rel="external" itemprop="url" title="NLnet"><img src="{$baseUrl}/img/spnsrs-overview/nlnet.png" alt="NLnet Foundation" id="nlnet"/></a>
</p>

<p>
<span itemprop="name">NLnet Foundation</span> financially supports organizations and people that contribute to an open information society. It funds software, events, educational activities &mdash; and more. The procedure is fast and open to anyone.
</p>

<p>
The articles of association for the NLnet Foundation state: <q cite="http://www.nlnet.nl/foundation/20070322-statuten.pdf">to promote the exchange of electronic information and all that is related or beneficial to that purpose</q>. NLnet believes in open standards and open source.
</p>

<p>
At the moment, dozens of projects and organizations are supported financially. Amongst them: research laboratory NLnet Labs, the Free Software Foundation, ThinkQuest, Intelligent Interactive Distributed Systems, and the Internet Society. 
</p>

<p>
Learn more at <a lang="en-GB" href="http://www.nlnet.nl/" rel="external" title="Ictivity">http://www.nlnet.nl/</a>.
</p>

</div>

<div class="grid_4" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a lang="nl-NL" href="http://www.ictivity.nl/" rel="external" itemprop="url" title="Ictivity"><img src="{$baseUrl}/img/spnsrs-overview/ictivity.png" alt="Ictivity" id="ictivity"/></a>
</p>

<p>
Learn more at <a lang="nl-NL" href="http://www.ictivity.nl/" rel="external" title="Ictivity">www.ictivity.nl</a> (Dutch).
</p>

</div>

<div class="grid_4" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a lang="en-GB" href="http://www.fluendo.com/" rel="external" itemprop="url" title="Fluendo"><img src="{$baseUrl}/img/spnsrs-overview/fluendo.png" alt="Fluendo" id="fluendo"/></a>
</p>

<p>
<strong>Fluendo and Flumotion provide the video streaming during the main conference.</strong>
</p>

<p>
Founded in 2004 in Barcelona, <span itemprop="name">Fluendo</span> specializes in software development and consulting services. Fluendo employs several of the central developers of GStreamer, an Open Source framework which is quickly establishing itself as the de-facto standard multimedia framework for GNU/Linux and UNIX systems.
</p>

<p>
Fluendo provides a wide range of products under and above GStreamer, including proprietary codecs (together with their respective patent licenses), a streaming server, Fluendo DVD Player, Fluendo Media Center, and Moovida Pro Media Player for Windows.
</p>

<p>
Fluendo develops multimedia software to offer cross-platform and open source-based multimedia solutions to computer devices and consumer electronics, always aiming at proposing an innovative perspective of entertainment.
</p>

<p>
Fluendo is one of three companies in the Fluendo Group, and was the first one to be created. Fluendo is the pioneer company of the group gathering GStreamer expertise, and also holding the basis of all the solutions offered by the other two companies: Flumotion Services S.A. and Fluendo Embedded S.L.
</p>

<p>
Learn more at <a lang="en-GB" href="http://www.fluendo.com/" rel="external" title="Fluendo">www.fluendo.com</a>.
</p>

</div>

<div class="grid_4 omega" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a lang="en-GB" href="http://www.flumotion.net/" rel="external" itemprop="url" title="Flumotion"><img src="{$baseUrl}/img/spnsrs-overview/flumotion.png" alt="Flumotion" id="flumotion"/></a>
</p>

<p>
<strong>Flumotion and Fluendo provide the video streaming during the main conference.</strong>
</p>

<p>
<span itemprop="name">Flumotion</span> offers multi-format streaming solutions for the delivery of audio and video services over the Internet. Started in 2006 by a group of open source developers and multimedia experts as a Free Software project, Flumotion combines an innovative open source software package with GStreamer.
</p>

<p>
Thanks to its flexible and distributed design and its choice of Python as the high-level language for development, it is now the world’s easiest and most powerful streaming software and platform for open streaming. Flumotion's end-to-end technology covers the entire streaming value chain and enables streaming in open as well as proprietary formats. Live and on-demand streaming for new formats and devices is integrated in record time such as WebM, iPad and Android. 
</p>

<p>
Learn more at <a lang="en-GB" href="http://www.flumotion.net/" rel="external" title="Ictivity">www.flumotion.net</a>.
</p>

</div>

<div class="clear"></div>

<div class="grid_4 alpha omega">
<h4>Media partner</h4>
</div>
<div class="grid_4">&nbsp;</div>
<div class="grid_4">&nbsp;</div>
<div class="grid_4 omega">&nbsp;</div>

<div class="clear"></div>

<div class="grid_4 alpha omega" itemscope itemtype="http://data-vocabulary.org/Organization">

<p class="heading">
<a href="http://www.linux-magazine.com/" rel="external" itemprop="url" title="Linux Magazine Online (English)"><img src="{$baseUrl}/img/spnsrs-overview/linux-magazine.png" alt="Linux Magazine" id="linux-magazine"/></a>
</p>
<p>
<span itemprop="name">Linux Magazine</span> is a monthly magazine serving readers in over 50 countries.
</p>
<p>
<abbr title="GNOME Users&#146; And Developers&#146; European Conference">GUADEC</abbr> 2010 is sponsored by Linux Magazine
    <a href="http://www.linux-magazine.com/" rel="external" itemprop="url" title="Linux Magazine Online (UK/International)">International</a>,
    <a lang="de-DE" href="http://www.linux-magazin.de/" rel="external" itemprop="url" title="Linux-Magazin Online (Deutschland)">Germany</a>,
    <a lang="pl-PL" href="http://www.linux-magazine.pl/" rel="external" itemprop="url" title="Linux Magazine (Polska)">Poland</a> and
    <a lang="es-ES" href="http://www.linux-magazine.es/" rel="external" itemprop="url" title="Linux Magazine (España)">Spain</a>.
</p>

</div>

<div class="clear"></div>

</div>

{include file="common/footer.tpl"}
