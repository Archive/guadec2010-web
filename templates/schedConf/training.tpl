{**
 * training.tpl
 *
 * Copyright (c) 2010 Dave Neary <dneary@gnome.org>
 * Copyright (c) 2010 Sense Hofstede <qense@ubuntu.com>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * GNOME training advertisement page.
 *
 *}
{assign var="pageTitleTranslated" value="GNOME developer training"}
{include file="common/header.tpl"}
{assign var="helpTopicId" value="user.conferenceInformation"}

<div id="training">
<p>
<a href="http://register.guadec.org/" title="GUADEC 2010 registration"><img src="{$baseUrl}/img/register_now.png" style="float: right;" alt="Register Now"/></a>
On 26 July and 27 July 2010, the first GNOME Developer Training sessions will be run as part of <abbr title="GNOME Users&#146; And Developers&#146; European Conference">GUADEC</abbr> 2010. The participants will be trained in developing on and for the GNOME desktop, and in working effectively with the community.
</p>

<p>
The GNOME Developer Training sessions at GUADEC are designed to give experienced software developers the tools that they need to get the most out of developing free software in an open community. We will cover common Linux development tools, the GNOME and FreeDesktop.org development platforms, and the social dynamics of contributing to community projects.
</p>

<p>
Improve the productivity of your development team with this two day course, which includes a hands-on practical session, and an in-depth overview of the social aspect of community development. Learn how to reduce maintenance costs, get your work upstream and influence the direction of upstream projects.
</p>

<p>
Conference attendance includes a full professional pass for GUADEC, the flagship annual conference of the GNOME project, giving you access to three days of presentations, <abbr title="Birds of a Feather (sessions)">BoFs</abbr> and tutorials, for a value of <em>€250</em>.
</p>

<h2>Pricing</h2>

<ul>
 <li>
    <strong>2 days training course + professional registration for <abbr title="GNOME Users&#146; And Developers&#146; European Conference">GUADEC</abbr> 2010 (value <em>€250</em>):</strong> <em>€1500</em>
 </li>
 <li>
    <strong>early bird price (for registrations before June 15th 2010):</strong> <em>€1200</em>
 </li>
</ul>

<p>
The pricing includes delivery of printed training material related to the course, and meals and refreshments during training.
</p>

<p>
To ensure the highest quality, space is strictly limited, we encourage you to register early to guarantee availability.
</p>

<p>
You can register for the GUADEC training course at <a href="http://register.guadec.org/" title="GUADEC 2010">http://guadec.org/</a>.
</p>

<h2>Syllabus</h2>

<p>The course is made up of four half-day modules, including:</p>

<h3>Developer tools and development environments:</h3>

<ul>
<li>Source control &mdash; Git, <abbr title="Subversion">SVN</abbr>, Bazaar</li>
<liUsing autotools</li>
<li>Standard GNOME build dependencies & their purposes (pkgconfig, intltool,
 gtk-doc)</li>
<li>Cross-compilation environments (Scratchbox, OpenEmbedded)</li>
<li>Debuggers (<abbr title="GNU Project Debugger">GDB</abbr>,
 <a href="http://projects.gnome.org/nemiver/" rel="external"
 title="Nemiver - A GNOME C/C++ Debugger">Nemiver</a>)</li>
<li>Valgrind (including memcheck, Massif, KCachegrind, Callgrind)</li>
<li>Other developer tools: sar, sysprof, bootchart</li>
</ul>

<h3>The GNOME platform:</h3>

<ul>
<li>GLib and GObject</li>
<li>GTK+ and Clutter</li>
<li>Using Glade and GtkBuilder</li>
<li>DBus, D-Feet, registering and calling DBus APIs</li>
<li>GConf</li>
<li>GStreamer</li>
<li>Telepathy</li>
<li>Language bindings &mdash; C++, Java, C#, Python, Vala</li>
</ul>

<h3>A hands-on Practical Workshop:</h3>

<ul>
<li>Set up a GNOME development environment</li>
<li>Write a complete GNOME application in Python</li>
<li>Integrate with the GNOME desktop</li>
</ul>

<h3>Community development</h3>

<ul>
<li>Community communication forums (mailing lists, forums, IRC, Bugzilla, source control)</li>
<li>Effective community participation, community social norms</li>
<li>Project governance &mdash; how things get done</li>
<li>Getting changes upstream</li>
<li>Getting to maintainer &mdash; how to make friends and influence people</li>
<li>Building a vibrant community</li>
</ul>
<hr/>
<h2>Contact</h2>
<p>For further queries or group rates, please contact Dave Neary at <em>dneary &lt;at sign&gt; gnome.org</em>.
</p>
<p>
GNOME Developer Training is run by <a href="http://www.neary-consulting.com/" rel="external" title="Neary Consulting">Neary Consulting</a> on behalf of <a href="http://foundation.gnome.org/" rel="external" title="The GNOME Foundation">The GNOME Foundation</a>.
<img src="{$baseUrl}/img/neary_consulting.png" alt="Neary Consulting" style="float: left; padding-right: 0.5em;"/>

<a href="http://register.guadec.org/" title="GUADEC 2010 registration"><img src="{$baseUrl}/img/register_now.png" style="float: right;" alt="Register Now"/></a>
</p>
</div>

{include file="common/footer.tpl"}
