{**
 * bio.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * RT Email Sent page.
 *
 * $Id: sent.tpl,v 1.4.2.1 2009/04/08 20:45:52 asmecher Exp $
 *}
{assign var=pageTitle value="email.email"}
{include file="rt/header.tpl"}

<p>{translate key="rt.email.sent"}</p>

{include file="rt/footer.tpl"}
