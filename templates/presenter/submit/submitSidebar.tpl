{**
 * submitSidebar.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Sidebar accompanying paper submission pages.
 *
 * $Id: submitSidebar.tpl,v 1.5.2.1 2009/04/08 20:45:52 asmecher Exp $
 *}
{* Empty for now *}
