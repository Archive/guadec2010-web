{**
 * stream.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Copyright (c) 2010 Sense Hofstede <sense@ubuntu.com>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Conference index page. Displayed when a conference, but not a scheduled
 * conference, has been selected.
 *
 * $Id: index.tpl,v 1.19.2.1 2009/04/08 20:45:51 asmecher Exp $
 *}
{assign var="pageTitleTranslated" value=$pageTitle}
{assign var="hideTitle" value="true"}
{include file="common/header.tpl"}

<div id="streamview">
    <div>
        <p style="float: left; ">
            <em>{$streamList}</em>
        </p>
        <p style="float: right;">
            <em style="font-size: 10pt;"><a rel="external" href="http://live.gnome.org/WebM" title="Information for playing WebM streams on your system">WebM support</a></em>
        </p>
    </div>

    <div class="clear"></div>

    <div class="videoplayer">
        <video width="560" height="340" autoplay="autoplay" controls="controls" type="video/webm" src="{$streamUrl}">Please try to open <a href="{$streamurl}" title="WebM Live Stream {$streamNumber} by Flumotion">{$streamurl}</a> in a WebM-capable video player, since you're browser doesn't seem to support the HTML5 video tag.</video>
    </div><br/ >
    <a href="http://www.flumotion.com/webm"><img src="{$baseUrl}/img/flumotion_banner.png" width="560" height="66" border="0" alt="WebM Live Streaming by Flumotion" title="WebM Live Streaming by Flumotion"/></a>
</div>

{include file="common/footer.tpl"}
