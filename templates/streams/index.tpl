{**
 * index.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Copyright (c) 2010 Sense Hofstede <sense@ubuntu.com>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Conference index page. Displayed when a conference, but not a scheduled
 * conference, has been selected.
 *
 * $Id: index.tpl,v 1.19.2.1 2009/04/08 20:45:51 asmecher Exp $
 *}
{assign var="pageTitleTranslated" value=$pageTitle}
{assign var="hideTitle" value="true"}
{include file="common/header.tpl"}

<div id="streams">

    <div id="streamlist">
        <p>
            <div class="a_stream">
                <a href="{url conference="guadec" schedConf="2010" page="streams" op="stream"}?number=1" title="Watch video live stream">Video Stream 'Paris'</a>
                <br/><em style="font-size: 8pt;">http://live1.guadec.stream.flumotion.com/guadec/live1.webm.m3u</em>
            </div>
            <div class="a_stream">            
                <a href="{url conference="guadec" schedConf="2010" page="streams" op="stream"}?number=2" title="Watch video live stream">Video Stream 'Copenhagen'</a>
                <br/><em style="font-size: 8pt;">http://live2.guadec.stream.flumotion.com/guadec/live2.webm.m3u</em>
            </div>
            <div class="a_stream">            
                <a href="{url conference="guadec" schedConf="2010" page="streams" op="stream"}?number=3" title="Watch video live stream">Video Stream 'Seville'</a>
                <br/><em style="font-size: 8pt;">http://live3.guadec.stream.flumotion.com/guadec/live3.webm.m3u</em>
            </div>
        </p>
    </div>

</div>

{include file="common/footer.tpl"}
