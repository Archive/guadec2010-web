{**
 * index.tpl
 *
 * Copyright (c) 2010 Sense Hofstede <qense@ubuntu.com>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * View the livestream
 *
 *}
{assign var="pageTitleTranslated" value="Conference livestream"}
{include file="common/header.small.tpl"}

{literal}
<script type="text/javascript">
$(document).ready(function(){ls.init("GUADEC", $("#t")[0])});
</script>
{/literal}

<table id="t" cellspacing="5"></table>

<p>
<em>Lifestream based on <a href="https://launchpad.net/udstream" title="UDStream on Launchpad">UDStream</a>, social media icons from <a href="http://icondock.com/free/vector-social-media-icons" title="Vector Social Media Icons - Icon Dock">'Vector Social Media Icons'</a> under the <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons &mdash;
      Attribution-Share Alike 3.0 Unported">Creative Commons Attribution-Share Alike 3.0 Unported License</a></em>
</p>

{include file="common/footer.tpl"}
