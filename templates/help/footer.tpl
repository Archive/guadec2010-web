{**
 * footer.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common footer for help pages.
 *
 * $Id: footer.tpl,v 1.5.2.1 2009/04/08 20:45:51 asmecher Exp $
 *}
{call_hook name="Templates::Help::Footer::PageFooter"}
</div>
</div>
</body>
</html>
