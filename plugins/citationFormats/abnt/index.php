<?php

/**
 * @defgroup plugins_citationFormats_abnt
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for ABNT citation plugin.
 *
 * @ingroup plugins_citationFormats_abnt
 */

//$Id: index.php,v 1.6.2.1 2009/04/08 20:45:46 asmecher Exp $

require_once('AbntCitationPlugin.inc.php');

return new AbntCitationPlugin();

?>
