<?php

/**
 * @defgroup plugins_citationFormats_bibtex
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for BibTeX citation plugin.
 *
 * @ingroup plugins_citationFormats_bibtex
 */

//$Id: index.php,v 1.6.2.1 2009/04/08 20:45:46 asmecher Exp $

require_once('BibtexCitationPlugin.inc.php');

return new BibtexCitationPlugin();

?>
