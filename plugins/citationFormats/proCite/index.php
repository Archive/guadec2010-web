<?php

/**
 * @defgroup plugins_citationFormats_proCite
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for ProCite citation plugin.
 *
 * @ingroup plugins_citationFormats_proCite
 */

//$Id: index.php,v 1.6.2.1 2009/04/08 20:45:47 asmecher Exp $

require_once('ProCiteCitationPlugin.inc.php');

return new ProCiteCitationPlugin();

?>
