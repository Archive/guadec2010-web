<?php

/**
 * @defgroup plugins_citationFormats_endNote
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for EndNote citation plugin.
 *
 * @ingroup plugins_citationFormats_endNote
 */

//$Id: index.php,v 1.6.2.1 2009/04/08 20:45:47 asmecher Exp $

require_once('EndNoteCitationPlugin.inc.php');

return new EndNoteCitationPlugin();

?>
