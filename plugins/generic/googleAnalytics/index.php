<?php

/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Wrapper for Google Analytics plugin.
 *
 * @package plugins.generic.googleAnalytics
 *
 * $Id: index.php,v 1.1.2.1 2009/04/08 20:45:48 asmecher Exp $
 */

require_once('GoogleAnalyticsPlugin.inc.php');

return new GoogleAnalyticsPlugin();

?>
