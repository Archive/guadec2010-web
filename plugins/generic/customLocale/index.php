<?php 

/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Wrapper for custom locale plugin. Plugin based on Translator plugin.
 *
 * @package plugins.generic.customLocale
 *
 * $Id: index.php,v 1.1.2.1 2009/04/08 20:45:48 asmecher Exp $
 */

require_once('CustomLocalePlugin.inc.php');

return new CustomLocalePlugin(); 

?> 
