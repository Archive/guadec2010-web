<?php

/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Wrapper for Announcement Feed plugin. Based on Web Feed Plugin.
 *
 * @package plugins.generic.announcementFeed
 *
 * $Id: index.php,v 1.1.2.1 2009/04/08 20:45:47 asmecher Exp $
 */

require_once('AnnouncementFeedPlugin.inc.php');

return new AnnouncementFeedPlugin(); 

?> 
