<?php

/**
 * @defgroup plugins_generic_tinymce
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for TinyMCE plugin.
 *
 * @ingroup plugins_generic_tinymce
 */

//$Id: index.php,v 1.2.2.1 2009/04/08 20:45:48 asmecher Exp $

require_once('TinyMCEPlugin.inc.php');

return new TinyMCEPlugin();

?> 
