<?php

/**
 * @defgroup plugins_generic_acron
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for acron plugin
 *
 * @ingroup plugins_generic_acron
 */

//$Id: index.php,v 1.7.2.1 2009/04/08 20:45:47 asmecher Exp $

require_once('AcronPlugin.inc.php');

return new AcronPlugin();

?> 
