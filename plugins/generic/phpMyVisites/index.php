<?php

/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Wrapper for phpMyVisites plugin.
 *
 * @package plugins.generic.phpMyVisites
 *
 * $Id: index.php,v 1.1.2.1 2009/04/08 20:45:48 asmecher Exp $
 */

require_once('PhpMyVisitesPlugin.inc.php');

return new PhpMyVisitesPlugin();

?>
