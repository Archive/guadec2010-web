{**
 * pageTag.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * phpMyVisites page tag.
 *
 * $Id: pageTag.tpl,v 1.2.2.1 2009/04/08 20:45:48 asmecher Exp $
 *}
<!-- phpMyVisites -->
<script type="text/javascript">
<!--
var a_vars = Array();
var pagename='';

var phpmyvisitesSite = "{$phpmvSiteId|escape}";
var phpmyvisitesURL = "{$phpmvUrl}/phpmyvisites.php";
//-->
</script>
<script language="javascript" src="{$phpmvUrl}/phpmyvisites.js" type="text/javascript"></script>
<!-- /phpMyVisites -->

