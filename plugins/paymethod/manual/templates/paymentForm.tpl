{**
 * paymentForm.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Manual payment page
 *
 *}
{assign var="pageTitle" value="plugins.paymethod.manual"}
{include file="common/header.tpl"}

<p>{$message|nl2br}</p>

{include file="common/footer.tpl"}
