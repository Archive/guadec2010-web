<?php

/**
 * @defgroup plugins_paymethod_manual
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for manual payment plugin.
 *
 * @ingroup plugins_paymethod_manual
 */

//$Id: index.php,v 1.7.2.1 2009/04/08 20:45:49 asmecher Exp $

require_once('ManualPaymentPlugin.inc.php');

return new ManualPaymentPlugin();

?> 
