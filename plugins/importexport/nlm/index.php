<?php

/**
 * @defgroup plugins_citationFormats_nlm
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for NLM Meeting Abstract export plugin.
 *
 * @ingroup plugins_citationFormats_nlm
 */

//$Id: index.php,v 1.7.2.1 2009/04/08 20:45:49 asmecher Exp $

require_once('NLMExportPlugin.inc.php');

return new NLMExportPlugin();

?>
