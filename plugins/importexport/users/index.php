<?php

/**
 * @defgroup plugins_importexport_users
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for XML user import/export plugin.
 *
 * @ingroup plugins_importexport_users
 */

//$Id: index.php,v 1.2.2.1 2009/04/08 20:45:49 asmecher Exp $

require_once('UserImportExportPlugin.inc.php');

return new UserImportExportPlugin();

?>
