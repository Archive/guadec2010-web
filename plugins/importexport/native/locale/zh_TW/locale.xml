<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE locale SYSTEM "../../../../../locale/locale.dtd">

<!--
  * locale.xml
  *
  * Copyright (c) 2000-2009 John Willinsky
  * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
  *
  * Localization strings for the en_US (U.S. English) locale.
  *
  * $Id: locale.xml,v 1.1.2.1 2009/05/26 20:08:50 mj Exp $
  -->

<locale name="en_US" full_name="U.S. English">
	<message key="plugins.importexport.native.displayName">文章XML 插件</message>
	<message key="plugins.importexport.native.description">文章輸入與輸出</message>

	<message key="plugins.importexport.native.cliUsage"><![CDATA[Usage: {$scriptName} {$pluginName} [command] ...
Commands:
	import [xmlFileName] [conference_path] [sched_conf_path] [user_name] ...
	export [xmlFileName] [conference_path] [sched_conf_path] papers [paperId1] [paperId2] ...
	export [xmlFileName] [conference_path] [sched_conf_path] paper [paperId]

Additional parameters are required for importing data as follows, depending
on the root node of the XML document.

If the root node is <paper> or <papers>, additional parameters are required.
The following formats are accepted:

{$scriptName} {$pluginName} import [xmlFileName] [conference_path]
	[sched_conf_path] [user_name] track_id [trackId]

{$scriptName} {$pluginName} import [xmlFileName] [conference_path]
	[sched_conf_path] [user_name] track_name [trackName]

{$scriptName} {$pluginName} import [xmlFileName] [conference_path]
	[sched_conf_path] [user_name] track_abbrev [trackAbbrev]]]></message>
	<message key="plugins.importexport.native.export">輸出資料</message>
	<message key="plugins.importexport.native.export.tracks">輸出主題</message>
	<message key="plugins.importexport.native.export.papers">輪出文章</message>

	<message key="plugins.importexport.native.selectPaper">選擇文章</message>

	<message key="plugins.importexport.native.import.papers">輪入文章</message>
	<message key="plugins.importexport.native.import.papers.description">您所輸入的檔案包含一篇 (以上) 的文章，您必須選擇這些文章要輸入的主題位置。同一個檔案內的文章只能輸入到同一主題內，如果您要將不同的文章輸入到不同的主題內，請重新依文章的主題重製 XML 檔案，再輸入。</message>

	<message key="plugins.importexport.native.import">Import Data</message>
	<message key="plugins.importexport.native.import.description"><![CDATA[This plugin supports data import based on the native.dtd Document Type Definition. Supported root nodes are <paper> and <papers>.]]></message>

	<message key="plugins.importexport.native.import.error">Import Error</message>
	<message key="plugins.importexport.native.import.error.description">One or more errors occurred during import. Please check to ensure that the format of the import file correctly matches the specification. Specific details of the import errors are listed below.</message>

	<message key="plugins.importexport.native.cliError">ERROR:</message>
	<message key="plugins.importexport.native.error.uploadFailed">The upload failed; please ensure that uploads are allowed on your server and that the file is not too big for your PHP and web server configuration.</message>
	<message key="plugins.importexport.native.error.unknownUser">The specified user, "{$userName}", does not exist.</message>
	<message key="plugins.importexport.native.error.unknownConference">The specified conference or scheduled conference path, "{$conferencePath}" or "{$schedConfPath}" (respectively), does not exist.</message>
	<message key="plugins.importexport.native.export.error.couldNotWrite">Could not write to the file "{$fileName}".</message>
	<message key="plugins.importexport.native.export.error.trackNotFound">No track matched the specifier "{$trackIdentifier}".</message>
	<message key="plugins.importexport.native.export.error.paperNotFound">No paper matched the specified paper ID "{$paperId}".</message>
	<message key="plugins.importexport.native.import.error.unsupportedRoot">This plugin does not support the supplied root node "{$rootName}". Please ensure that the file is correctly formed and try again.</message>

	<message key="plugins.importexport.native.import.error.invalidBooleanValue">An invalid boolean value "{$value}" was specified. Please use "true" or "false".</message>
	<message key="plugins.importexport.native.import.error.invalidDate">An invalid date "{$value}" was specified.</message>
	<message key="plugins.importexport.native.import.error.unknownEncoding">Data was embedded using an unknown encoding type "{$type}".</message>
	<message key="plugins.importexport.native.import.error.couldNotWriteFile">Unable to save a local copy of "{$originalName}".</message>
	<message key="plugins.importexport.native.import.error.illegalUrl">The specified URL "{$url}" was illegal. Web-submitted imports support only http, https, ftp, or ftps methods.</message>
	<message key="plugins.importexport.native.import.error.unknownSuppFileType">An unknown supplementary file type "{$suppFileType}" was specified.</message>
	<message key="plugins.importexport.native.import.error.couldNotCopy">A specified URL "{$url}" could not be copied to a local file.</message>
	<message key="plugins.importexport.native.import.error.paperTitleLocaleUnsupported">An paper title ("{$paperTitle}") was provided in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperAbstractLocaleUnsupported">A paper abstract was provided for the paper "{$paperTitle}" in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.galleyLabelMissing">The paper "{$paperTitle}" in the track "{$trackTitle}" was missing a galley label.</message>
	<message key="plugins.importexport.native.import.error.suppFileMissing">The paper "{$paperTitle}" in the track "{$trackTitle}" was missing a supplementary file.</message>
	<message key="plugins.importexport.native.import.error.galleyFileMissing">The paper "{$paperTitle}" in the track "{$trackTitle}" was missing a galley file.</message>

	<message key="plugins.importexport.native.import.error.trackTitleLocaleUnsupported">A track title ("{$trackTitle}") was provided in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.trackAbbrevLocaleUnsupported">A track abbreviation ("{$trackAbbrev}") was provided in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.trackIdentifyTypeLocaleUnsupported">A track identifying type ("{$trackIdentifyType}") was provided in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.trackPolicyLocaleUnsupported">A track policy ("{$trackPolicy}") was provided in a locale ("{$locale}") that this conference does not support.</message>

	<message key="plugins.importexport.native.import.error.trackTitleMismatch">The track title "{$track1Title}" and the track title "{$track2Title}" matched the different existing conference tracks.</message>
	<message key="plugins.importexport.native.import.error.trackTitleMatch">The track title "{$trackTitle}" matched an existing conference track, but another title of this track doesn't match with another title of the existing conference track.</message>
	<message key="plugins.importexport.native.import.error.trackAbbrevMismatch">The track abbreviation "{$track1Abbrev}" and the track abbreviation "{$track2Abbrev}" matched the different existing conference tracks.</message>
	<message key="plugins.importexport.native.import.error.trackAbbrevMatch">The track abbreviation "{$trackAbbrev}" matched an existing conference track, but another abbreviation of this track doesn't match with another abbreviation of the existing conference track.</message>

	<message key="plugins.importexport.native.import.error.paperDisciplineLocaleUnsupported">An paper discipline was provided for the paper "{$paperTitle}" in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperTypeLocaleUnsupported">An paper type was provided for the paper "{$paperTitle}" in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperSubjectLocaleUnsupported">An paper subject was provided for the paper "{$paperTitle}" in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperSubjectClassLocaleUnsupported">An paper subject classification was provided for the paper "{$paperTitle}" in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperCoverageGeoLocaleUnsupported">An paper geographical coverage was provided for the paper "{$paperTitle}" in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperCoverageChronLocaleUnsupported">An paper geographical coverage was provided for the paper "{$paperTitle}" in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperCoverageSampleLocaleUnsupported">An paper sample coverage was provided for the paper "{$paperTitle}" in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperSponsorLocaleUnsupported">An paper sponsor was provided for the paper "{$paperTitle}" in a locale ("{$locale}") that this conference does not support.</message>

	<message key="plugins.importexport.native.import.error.paperAuthorCompetingInterestsLocaleUnsupported">An author competing interest was provided for the author "{$authorFullName}" of the paper "{$paperTitle}" in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperAuthorBiographyLocaleUnsupported">An author biography was provided for the author "{$authorFullName}" of the paper "{$paperTitle}" in a locale ("{$locale}") that this conference does not support.</message>

	<message key="plugins.importexport.native.import.error.galleyLocaleUnsupported">A galley of the paper "{$paperTitle}" was provided in a locale ("{$locale}") that this conference does not support.</message>

	<message key="plugins.importexport.native.import.error.paperSuppFileTitleLocaleUnsupported">A supplementary file title ("{$suppFileTitle}") of the paper "{$paperTitle}" was provided in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperSuppFileCreatorLocaleUnsupported">A creator of the supplementary file "{$suppFileTitle}" of the paper "{$paperTitle}" was provided in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperSuppFileSubjectLocaleUnsupported">A subject of the supplementary file "{$suppFileTitle}" of the paper "{$paperTitle}" was provided in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperSuppFileTypeOtherLocaleUnsupported">A custom type of the supplementary file "{$suppFileTitle}" of the paper "{$paperTitle}" was provided in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperSuppFileDescriptionLocaleUnsupported">A description of the supplementary file "{$suppFileTitle}" of the paper "{$paperTitle}" was provided in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperSuppFilePublisherLocaleUnsupported">A publisher of the supplementary file "{$suppFileTitle}" of the paper "{$paperTitle}" was provided in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperSuppFileSponsorLocaleUnsupported">A sponsor of the supplementary file "{$suppFileTitle}" of the paper "{$paperTitle}" was provided in a locale ("{$locale}") that this conference does not support.</message>
	<message key="plugins.importexport.native.import.error.paperSuppFileSourceLocaleUnsupported">A source of the supplementary file "{$suppFileTitle}" of the paper "{$paperTitle}" was provided in a locale ("{$locale}") that this conference does not support.</message>

	<message key="plugins.importexport.native.import.success">Import Successful</message>
	<message key="plugins.importexport.native.import.success.description">The import was successful. Successfully-imported items are listed below.</message>

</locale>
