<?php

/**
 * @defgroup plugins_blocks_role
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for role block plugin.
 *
 * @ingroup plugins_blocks_role
 */

//$Id: index.php,v 1.3.2.1 2009/04/08 20:45:46 asmecher Exp $

require_once('RoleBlockPlugin.inc.php');

return new RoleBlockPlugin();

?> 
