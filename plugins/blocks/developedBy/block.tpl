{**
 * block.tpl
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site sidebar menu -- "Developed By" block.
 *
 * $Id: block.tpl,v 1.4.2.1 2009/04/08 20:45:45 asmecher Exp $
 *}
<div class="block" id="sidebarDevelopedBy">
	<a class="blockTitle" href="http://pkp.sfu.ca/ocs/" id="developedBy">{translate key="common.openConferenceSystems"}</a>
</div>	
