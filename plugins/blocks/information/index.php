<?php

/**
 * index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins
 *
 * @brief Wrapper for information block plugin.
 */

//$Id: index.php,v 1.3.2.1 2009/04/08 20:45:46 asmecher Exp $

require_once('InformationBlockPlugin.inc.php');

return new InformationBlockPlugin();

?> 
