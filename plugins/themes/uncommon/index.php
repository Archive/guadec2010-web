<?php

/**
 * @defgroup plugins_themes_uncommon
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for "uncommon" theme plugin.
 *
 * @ingroup plugins_themes_uncommon
 */

//$Id: index.php,v 1.6.2.1 2009/04/08 20:45:50 asmecher Exp $

require_once('UncommonThemePlugin.inc.php');

return new UncommonThemePlugin();

?>
