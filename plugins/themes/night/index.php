<?php

/**
 * @defgroup plugins_themes_night
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for "night" theme plugin.
 *
 * @ingroup plugins_themes_night
 */

//$Id: index.php,v 1.6.2.1 2009/04/08 20:45:50 asmecher Exp $

require_once('NightThemePlugin.inc.php');

return new NightThemePlugin();

?>
