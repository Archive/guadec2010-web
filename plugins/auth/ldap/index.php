<?php

/**
 * @defgroup plugins_auth_ldap
 */
 
/**
 * @file plugins/auth/ldap/index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_auth_ldap
 * @brief Wrapper for loading the LDAP authentiation plugin.
 *
 */

// $Id: index.php,v 1.1.2.2 2009/04/08 20:45:45 asmecher Exp $


require_once('LDAPAuthPlugin.inc.php');

return new LDAPAuthPlugin();

?>
