<?php

/**
 * @defgroup plugins_reports_registrant
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for registrant report plugin.
 *
 * @ingroup plugins_reports_registrant
 */

//$Id: index.php,v 1.3.2.1 2009/04/08 20:45:50 asmecher Exp $

require_once('RegistrantReportPlugin.inc.php');

return new RegistrantReportPlugin();

?>
