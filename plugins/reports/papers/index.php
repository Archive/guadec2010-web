<?php

/**
 * @defgroup plugins_reports_paper
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for paper report plugin.
 *
 * @ingroup plugins_reports_paper
 */

//$Id: index.php,v 1.2.2.1 2009/04/08 20:45:50 asmecher Exp $

require_once('PaperReportPlugin.inc.php');

return new PaperReportPlugin();

?>
