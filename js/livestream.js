/**
 * livestream.js
 *
 * Copyright (c) 2009, Canonical Ltd, thanks to the Ubuntu Website team
 *
 * Functions for the livestream
 */

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && (typeof date == "string" || date instanceof String) && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date();
		if (isNaN(date)) throw new SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};

ls = {
  tag_api: [
    ["http://search.twitter.com/search.json?callback=?&q=%23$TAG", "twittersearch"],
    ["http://api.flickr.com/services/feeds/photos_public.gne?tags=$TAG&lang=en-us&format=json&jsoncallback=?", "flickr"],
    ["http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20rss%20where%20url%3D%22http%3A%2F%2Fidenti.ca%2Ftag%2F$TAG%2Frss%22&format=json&callback=?", "identicasearch"],
    ["http://gdata.youtube.com/feeds/videos?alt=json-in-script&vq=$TAG&callback=?", "gdata"],
    ["http://picasaweb.google.com/data/feed/api/all?alt=json-in-script&q=$TAG&callback=?", "gdata"],
  ],
  init: function(tag, element) {
    if (element && element.nodeName.toLowerCase() == "table") {
      ls.element = element;
    } else {
      ls.element = document.createElement("table");
      document.body.appendChild(ls.element);
    }
    if (tag) {
      for (var i=0; i<ls.tag_api.length; i++) {
        var api_url = ls.tag_api[i][0].replace("$TAG", tag);
        var api_callback_function = ls[ls.tag_api[i][1]];
        $.getJSON(api_url, 
            // closure to save this api_cb
            (function(api_cb) { 
                return function(data, textStatus) {
                    api_cb(data,textStatus);
                    ls.postProcess();
                }
            })(api_callback_function)
        );
      }
    }
    setTimeout(function() {ls.init(tag, element)}, 30000);
  },
  postProcess: function() {
      $(ls.element).find("tr:odd").removeClass("odd").removeClass("even").addClass("odd");
      $(ls.element).find("tr:even").removeClass("odd").removeClass("even").addClass("even");
      $(ls.element).find("td:odd").removeClass("odd").removeClass("even").addClass("odd");
      $(ls.element).find("td:even").removeClass("odd").removeClass("even").addClass("even");
      $(ls.element).linkify();
  },
  addItem: function(username, text, time, src, usrlink, link, thumbnail) {
    // time should be in ISO 2008-12-11T02:30:43-08:00 format
    // check this isn't already in here somewhere
    var foundit = false;
    $.each($(ls.element).find("tr"), function() {
        if ($.data(this, "link") == link) {
            foundit = true;
        }
    });
    if (foundit) return;
    
    var tr = document.createElement("tr");
    $.data(tr, "time", time);
    $.data(tr, "userlink", usrlink);
    $.data(tr, "link", link);
    var timetd = document.createElement("td");
    var timelink = document.createElement("a");
    timelink.href = link;
    timelink.appendChild(document.createTextNode(time.substr(11,5)));
    timetd.appendChild(timelink);
    timetd.className = "time";
    tr.appendChild(timetd);
    var usertd = document.createElement("td");
    var userlink = document.createElement("a");
    userlink.href = usrlink;
    userlink.appendChild(document.createTextNode(username));
    usertd.appendChild(userlink);
    usertd.className = "userlink";
    tr.appendChild(usertd);
    var texttd = document.createElement("td");
    texttd.className = "data";
    texttd.appendChild(document.createTextNode(text));
    if(thumbnail) {
      texttd.img = document.createElement('img');
      texttd.img.src = thumbnail ;
      texttd.img.style.display = "none";
      texttd.appendChild(texttd.img);

      $(texttd).hover(function(){
        this.img.style.position = "absolute";
        this.img.style.top = (this.offsetTop + this.offsetHeight) + "px";
        this.img.style.left = (this.offsetLeft + this.offsetWidth - this.img.width) + "px";
        $(this.img).fadeIn('fast');
      }, function(){
        $(this.img).fadeOut('fast');
      });
    }
    tr.appendChild(texttd);
    var srctd = document.createElement("td");
    srctd.appendChild(document.createTextNode(src));
    srctd.className = "source";
    tr.className = src;
    tr.appendChild(srctd);

    var found = false;
    $.each($(ls.element).find("tr"), function() {
      if (found) return;
      if ($.data(this, "time") < time) {
        ls.element.insertBefore(tr, this);
        found = true;
      }
    });
    if (!found) {
        ls.element.appendChild(tr);
    }

  },

  twittersearch: function(data) {
    $.each(data.results, function() {
      var time = new Date(Date.parse(this.created_at)).format("isoDateTime");
      ls.addItem(this.from_user, this.text, time, "twitter", "http://twitter.com/" + this.from_user,
        "http://twitter.com/" + this.from_user + "/status/" + this.id);
    });
  },
  identicasearch: function(data) {
    $.each(data.query.results.item, function() {
      ls.addItem(this.creator, this.title, this.date, "identica", this.maker.resource, this.link);
    });
  },
  flickr: function(data) {
    $.each(data.items, function() {
      var userlink = "http://www.flickr.com/people/" + this.author_id;
      ls.addItem(this.author, this.title, this.date_taken, "flickr", userlink,
        this.link, this.media.m);
    });
  },
  gdata: function(data) {
    $.each(data.feed.entry, function() {

      var links = $.grep(this['link'], function(e) {
        return (e['type'] == 'text/html' && e['rel'] == 'alternate');
      });
      var link = links[0]['href'];

      var nr_thumbnails = this['media$group']['media$thumbnail'].length;
      var thumbnail = this['media$group']['media$thumbnail'][nr_thumbnails-1]['url'];

      var time = this['published']['$t'];

      var kind = $.grep(this['category'], function(e) {
        return e['scheme'] == 'http://schemas.google.com/g/2005#kind';
      });
      var data_source;
      switch(kind[0]['term']) {
        case 'http://gdata.youtube.com/schemas/2007#video':
          data_source = 'youtube';
          author_link = "http://www.youtube.com/user/" + this['author'][0]['name']['$t'];
          break;
        case 'http://schemas.google.com/photos/2007#photo':
          data_source = 'picasa';
          author_link = "http://picasaweb.google.com/" + this['author'][0]['gphoto$user']['$t'];
          break;
        default:
          data_source = 'unknown';
          author_link = "";
      }

      ls.addItem(this['author'][0]['name']['$t'], this['media$group']['media$title']['$t'], time, data_source, author_link, link, thumbnail);
    });
  },
};

// Define: Linkify plugin
(function($){
  var url1 = /(^|&lt;|\s)(www\..+?\..+?)(\s|&gt;|$)/g,
      url2 = /(^|&lt;|\s)(((https?|ftp):\/\/|mailto:).+?)(\s|&gt;|$)/g,
      linkifyThis = function () {
        var childNodes = this.childNodes,
            i = childNodes.length;
        while(i--)
        {
          var n = childNodes[i];
          if (n.nodeType == 3) {
            var html = n.nodeValue;
            if (html)
            {
              html = html.replace(/</g, '&lt;')
                         .replace(/>/g, '&gt;')
                         .replace(url1, '$1<a href="http://$2">$2</a>$3')
                         .replace(url2, '$1<a href="$2">$2</a>$5');
              $(n).after(html).remove();
            }
          }
          else if (n.nodeType == 1  &&  !/^(a|button|textarea)$/i.test(n.tagName)) {
            linkifyThis.call(n);
          }
        }
      };

  $.fn.linkify = function () {
    return this.each(linkifyThis);
  };

})(jQuery);
