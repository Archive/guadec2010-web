<?php

/**
 * @file SubmissionCommentsHandler.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class SubmissionCommentsHandler
 * @ingroup pages_presenter
 *
 * @brief Handle requests for submission comments. 
 */

//$Id: SubmissionCommentsHandler.inc.php,v 1.8.2.1 2009/04/08 20:45:45 asmecher Exp $

import('pages.presenter.TrackSubmissionHandler');

class SubmissionCommentsHandler extends PresenterHandler {

	/**
	 * View director decision comments.
	 */
	function viewDirectorDecisionComments($args) {
		PresenterHandler::validate();
		PresenterHandler::setupTemplate(true);

		$paperId = $args[0];

		list($conference, $schedConf, $presenterSubmission) = TrackSubmissionHandler::validate($paperId);
		PresenterAction::viewDirectorDecisionComments($presenterSubmission);
	}

	/**
	 * Email a director decision comment.
	 */
	function emailDirectorDecisionComment() {
		$paperId = (int) Request::getUserVar('paperId');
		list($conference, $schedConf, $submission) = TrackSubmissionHandler::validate($paperId);

		parent::setupTemplate(true);		
		if (PresenterAction::emailDirectorDecisionComment($submission, Request::getUserVar('send'))) {
			Request::redirect(null, null, null, 'submissionReview', array($paperId));
		}
	}

	/**
	 * Edit comment.
	 */
	function editComment($args) {
		PresenterHandler::validate();
		PresenterHandler::setupTemplate(true);

		$paperId = $args[0];
		$commentId = $args[1];

		list($conference, $schedConf, $presenterSubmission) = TrackSubmissionHandler::validate($paperId);
		list($comment) = SubmissionCommentsHandler::validate($commentId);

		if ($comment->getCommentType() == COMMENT_TYPE_DIRECTOR_DECISION) {
			// Cannot edit a director decision comment.
			Request::redirect(null, null, Request::getRequestedPage());
		}

		PresenterAction::editComment($presenterSubmission, $comment);

	}

	/**
	 * Save comment.
	 */
	function saveComment() {
		PresenterHandler::validate();
		PresenterHandler::setupTemplate(true);

		$paperId = Request::getUserVar('paperId');
		$commentId = Request::getUserVar('commentId');

		// If the user pressed the "Save and email" button, then email the comment.
		$emailComment = Request::getUserVar('saveAndEmail') != null ? true : false;

		list($conference, $schedConf, $presenterSubmission) = TrackSubmissionHandler::validate($paperId);
		list($comment) = SubmissionCommentsHandler::validate($commentId);

		if ($comment->getCommentType() == COMMENT_TYPE_DIRECTOR_DECISION) {
			// Cannot edit a director decision comment.
			Request::redirect(null, null, Request::getRequestedPage());
		}

		PresenterAction::saveComment($presenterSubmission, $comment, $emailComment);

		$paperCommentDao = &DAORegistry::getDAO('PaperCommentDAO');
		$comment = &$paperCommentDao->getPaperCommentById($commentId);

		// Redirect back to initial comments page
		if ($comment->getCommentType() == COMMENT_TYPE_DIRECTOR_DECISION) {
			Request::redirect(null, null, null, 'viewDirectorDecisionComments', $paperId);
		}
	}

	/**
	 * Delete comment.
	 */
	function deleteComment($args) {
		PresenterHandler::validate();
		PresenterHandler::setupTemplate(true);

		$paperId = $args[0];
		$commentId = $args[1];

		$paperCommentDao = &DAORegistry::getDAO('PaperCommentDAO');
		$comment = &$paperCommentDao->getPaperCommentById($commentId);

		list($conference, $schedConf, $presenterSubmission) = TrackSubmissionHandler::validate($paperId);
		list($comment) = SubmissionCommentsHandler::validate($commentId);
		PresenterAction::deleteComment($commentId);

		// Redirect back to initial comments page
		if ($comment->getCommentType() == COMMENT_TYPE_DIRECTOR_DECISION) {
			Request::redirect(null, null, null, 'viewDirectorDecisionComments', $paperId);
		}
	}


	//
	// Validation
	//

	/**
	 * Validate that the user is the presenter of the comment.
	 */
	function validate($commentId) {
		parent::validate();

		$isValid = true;

		$paperCommentDao = &DAORegistry::getDAO('PaperCommentDAO');
		$user = &Request::getUser();

		$comment = &$paperCommentDao->getPaperCommentById($commentId);

		if ($comment == null) {
			$isValid = false;

		} else if ($comment->getAuthorId() != $user->getUserId()) {
			$isValid = false;
		}

		if (!$isValid) {
			Request::redirect(null, null, Request::getRequestedPage());
		}

		return array($comment);
	}
}
?>
