<?php

/**
 * @defgroup pages_livestreamscreen
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Copyright (c) 2010 Sense Hofstede <qense@ubuntu.com>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Show a live feed of microblogging activity
 *
 * @ingroup pages_livestreamscreen
 */

define('HANDLER_CLASS', 'LivestreamscreenHandler');

import('pages.livestreamscreen.LivestreamscreenHandler');

?>
