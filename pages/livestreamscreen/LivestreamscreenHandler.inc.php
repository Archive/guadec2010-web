<?php

/**
 * @file LivestreamHandler.inc.php
 *
 * Copyright (c) 2010 Sense Hofstede <qense@ubuntu.com>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class LivestreamscreenHandler
 * @ingroup pages_livestreamscreen
 *
 * @brief Show a live feed of microblogging activity
 */


class LivestreamscreenHandler extends Handler {

	/**
	 * Display livestream index page.
	 */
	function index() {
		parent::validate();

		$templateMgr = &TemplateManager::getManager();
		$templateMgr->setCacheability(CACHEABILITY_PUBLIC);

		$templateMgr->append('stylesheets', Request::getBaseUrl() . "/styles/livestreamscreen.css");
		$templateMgr->append('javascripts', "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js");
		$templateMgr->append('javascripts', Request::getBaseUrl() . "/js/livestream.js");

		$templateMgr->assign('pageTitle', "Conference livestream");
	
		$templateMgr->display('livestreamscreen/index.tpl');
	}
}

?>
