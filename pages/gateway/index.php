<?php

/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Handle gateway interaction requests. 
 *
 * @package pages.gateway
 *
 * $Id: index.php,v 1.1.2.1 2009/04/08 20:45:45 asmecher Exp $
 */

define('HANDLER_CLASS', 'GatewayHandler');

import('pages.gateway.GatewayHandler');

?>
