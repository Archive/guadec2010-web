<?php

/**
 * @defgroup pages_livestream
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Copyright (c) 2010 Sense Hofstede <qense@ubuntu.com>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Show a live feed of microblogging activity
 *
 * @ingroup pages_livestream
 */

define('HANDLER_CLASS', 'LivestreamHandler');

import('pages.livestream.LivestreamHandler');

?>
