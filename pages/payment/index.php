<?php

/**
 * @defgroup pages_payment
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Handle requests for interactions between the payment system and external
 * sites/systems.
 *
 * @ingroup pages_payment
 */

//$Id: index.php,v 1.4.2.1 2009/04/08 20:45:45 asmecher Exp $

define('HANDLER_CLASS', 'PaymentHandler');

import('pages.payment.PaymentHandler');

?>
