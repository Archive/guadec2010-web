<?php

/**
 * @defgroup pages_streams
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Copyright (c) 2010 Sense Hofstede <sense@ubuntu.com>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Show a live feed of microblogging activity
 *
 * @ingroup pages_streams
 */

define('HANDLER_CLASS', 'StreamsHandler');

import('pages.streams.StreamsHandler');

?>
