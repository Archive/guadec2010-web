<?php

/**
 * @file StreamsHandler.inc.php
 *
 * Copyright (c) 2010 Sense Hofstede <sense@ubuntu.com>
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class StreamsHandler
 * @ingroup pages_streams
 *
 * @brief Make it possible to watch the three streams
 */

function getStreamUrl($streamlist) {
    /* START Flumotion streaming */
    // create curl resource
    $ch = curl_init();

    // set url
    curl_setopt($ch, CURLOPT_URL, $streamlist);

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);
    
    $lines = explode("\n", $output);
    $streamurl = $lines[2];
    /* END Flumotion streaming */

    return $streamurl;
}

class StreamsHandler extends Handler {

	/**
	 * Display the choice of the three streams
	 */
	function index() {
		parent::validate();

		$templateMgr = &TemplateManager::getManager();
		$templateMgr->setCacheability(CACHEABILITY_PUBLIC);

		$templateMgr->append('stylesheets', Request::getBaseUrl() . "/styles/streams.css");
		$templateMgr->assign('pageTitle', "Live Video Streams");
	
		$templateMgr->display('streams/index.tpl');
	}

    /**
     Show a certain stream
     */
    function stream() {
        $streamNumber = $_GET['number'];
        if (is_numeric($streamNumber) && (int)$streamNumber >= 1 && (int)$streamNumber <= 3) {
            parent::validate();
            $streamList = sprintf("http://live%d.guadec.stream.flumotion.com/guadec/live%d.webm.m3u", $streamNumber, $streamNumber);
            switch ((int)$streamNumber) {
                case 1:
                    $streamRoom = "Paris";
                    break;
                case 2:
                    $streamRoom = "Copenhagen";
                    break;
                case 3:
                    $streamRoom = "Seville";
                    break;
            }
            
		    $templateMgr = &TemplateManager::getManager();

		    $templateMgr->append('stylesheets', Request::getBaseUrl() . "/styles/streams.css");
		    $templateMgr->assign('pageTitle', sprintf("Live Video Stream '%s'", $streamRoom));

            $templateMgr->assign('streamList', $streamList);
		    $templateMgr->assign('streamUrl', getStreamUrl($streamList));
		    $templateMgr->assign('streamNumber', $streamNumber);

        	$templateMgr->assign('pageHierarchy', array(
    			array(Request::url('guadec', '2010', 'streams'), "Live Video Streams", true)));

		    $templateMgr->display('streams/stream.tpl');
        } else {
        }
    }
}
?>
