<?php

/**
 * @defgroup pages_conference
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Handle conference index requests. 
 *
 * @ingroup pages_conference
 */

//$Id: index.php,v 1.5.2.1 2009/04/08 20:45:45 asmecher Exp $

define('HANDLER_CLASS', 'ConferenceHandler');

import('pages.conference.ConferenceHandler');

?>
