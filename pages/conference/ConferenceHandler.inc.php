<?php

/**
 * @file ConferenceHandler.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class ConferenceHandler
 * @ingroup pages_index
 *
 * @brief Handle conference index requests.
 */

//$Id: ConferenceHandler.inc.php,v 1.14.2.1 2009/04/08 20:45:45 asmecher Exp $

class ConferenceHandler extends Handler {

	/**
	 * Display the home page for the current conference.
	 */
	function index($args) {
		list($conference, $schedConf) = parent::validate(true, false);
    
        /* START Flumotion streaming */
        // create curl resource
        $ch = curl_init();

        // set url
        $streamNumber = 1;
        $streamList = sprintf("http://live%d.guadec.stream.flumotion.com/guadec/live%d.webm.m3u", $streamNumber, $streamNumber);
        curl_setopt($ch, CURLOPT_URL, $streamList);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
        
        $lines = explode("\n", $output);
        $streamurl = $lines[2];
        /* END Flumotion streaming */

		$templateMgr = &TemplateManager::getManager();

		$conferenceDao = &DAORegistry::getDAO('ConferenceDAO');

		$templateMgr->assign('helpTopicId', 'user.home');

		// Assign header and content for home page
		$templateMgr->assign('displayPageHeaderTitle', $conference->getPageHeaderTitle(true));
		$templateMgr->assign('displayPageHeaderLogo', $conference->getPageHeaderLogo(true));
		$templateMgr->assign('additionalHomeContent', $conference->getLocalizedSetting('additionalHomeContent'));
		$templateMgr->assign('homepageImage', $conference->getLocalizedSetting('homepageImage'));
		$templateMgr->assign('description', $conference->getLocalizedSetting('description'));
		$templateMgr->assign('conferenceTitle', $conference->getConferenceTitle());
        switch ((int)$streamNumber) {
            case 1:
                $streamRoom = "Paris";
                break;
            case 2:
                $streamRoom = "Copenhagen";
                break;
            case 3:
                $streamRoom = "Seville";
                break;
        }
        $templateMgr->assign('streamList', $streamList);
		$templateMgr->assign('streamNumber', $streamNumber); 
        $templateMgr->assign('streamRoom', $streamRoom);     
		$templateMgr->assign('streamurl', $streamurl);

		$schedConfDao = &DAORegistry::getDAO('SchedConfDAO');
		$currentSchedConfs = &$schedConfDao->getCurrentSchedConfs($conference->getConferenceId());
//		$pastSchedConfs = &$schedConfDao->getEnabledSchedConfs($conference->getConferenceId());

		$templateMgr->assign_by_ref('currentSchedConfs', $currentSchedConfs);
//		$templateMgr->assign_by_ref('pastSchedConfs', $pastSchedConfs);

		$enableAnnouncements = $conference->getSetting('enableAnnouncements');
		if ($enableAnnouncements) {
			$enableAnnouncementsHomepage = $conference->getSetting('enableAnnouncementsHomepage');
			if ($enableAnnouncementsHomepage) {
				$numAnnouncementsHomepage = $conference->getSetting('numAnnouncementsHomepage');
				$announcementDao = &DAORegistry::getDAO('AnnouncementDAO');
				$announcements = &$announcementDao->getNumAnnouncementsNotExpiredByConferenceId($conference->getConferenceId(), 0, $numAnnouncementsHomepage);
				$templateMgr->assign('announcements', $announcements);
				$templateMgr->assign('enableAnnouncementsHomepage', $enableAnnouncementsHomepage);
			}
		} 
		$templateMgr->display('conference/index.tpl');
	}
}

?>
