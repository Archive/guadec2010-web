<?php

/**
 * @defgroup pages_help
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Handle requests for viewing help pages.
 *
 * @ingroup pages_help
 */

//$Id: index.php,v 1.5.2.1 2009/04/08 20:45:45 asmecher Exp $

define('HANDLER_CLASS', 'HelpHandler');

import('pages.help.HelpHandler');

?>
