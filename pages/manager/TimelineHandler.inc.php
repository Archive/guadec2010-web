<?php

/**
 * @file TimelineHandler.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class TimelineHandler
 * @ingroup pages_manager
 *
 * @brief Handle requests for scheduled conference timeline management functions. 
 */

//$Id: TimelineHandler.inc.php,v 1.5.2.1 2009/04/08 20:45:45 asmecher Exp $

class TimelineHandler extends ManagerHandler {

	/**
	 * Display a list of the tracks within the current conference.
	 */
	function timeline($args) {
		parent::validate();
		parent::setupTemplate(true);

		import('manager.form.TimelineForm');

		$timelineForm = &new TimelineForm(Request::getUserVar('overrideDates'));
		$timelineForm->initData();
		$timelineForm->display();

	}

	function updateTimeline($args) {
		parent::validate();

		import('manager.form.TimelineForm');

		$timelineForm = &new TimelineForm(Request::getUserVar('overrideDates'));
		$timelineForm->readInputData();

		if ($timelineForm->validate()) {
			$timelineForm->execute();
			Request::redirect(null, null, null, 'index');
		} else {
			parent::setupTemplate(true);
			$timelineForm->setData('errorsExist', true);
			$timelineForm->display();
		}
	}

}
?>
