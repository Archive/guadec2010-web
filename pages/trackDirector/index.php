<?php

/**
 * @defgroup pages_trackDirector
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Handle requests for track director functions.
 *
 * @ingroup pages_trackDirector
 */

//$Id: index.php,v 1.5.2.1 2009/04/08 20:45:45 asmecher Exp $

define('HANDLER_CLASS', 'TrackDirectorHandler');

import('pages.trackDirector.TrackDirectorHandler');

?>
