<?php

/**
 * @defgroup pages_comment
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Handle requests for comment functions. 
 *
 * @ingroup pages_comment
 */

//$Id: index.php,v 1.5.2.1 2009/04/08 20:45:45 asmecher Exp $

define('HANDLER_CLASS', 'CommentHandler');

import('pages.comment.CommentHandler');

?>
