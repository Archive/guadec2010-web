<?php

/**
 * @defgroup pages_oai
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Handle Open Archives Initiative protocol interaction requests.
 *
 * @ingroup pages_oai
 */

//$Id: index.php,v 1.5.2.1 2009/04/08 20:45:45 asmecher Exp $

define('HANDLER_CLASS', 'OAIHandler');

import('pages.oai.OAIHandler');

?>
