<?php

/**
 * @defgroup currency
 */
 
/**
 * @file Currency.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class Currency
 * @ingroup currency 
 * @see CurrencyDAO
 *
 * @brief Basic class describing a currency.
 */

//$Id: Currency.inc.php,v 1.6.2.1 2009/04/08 20:45:42 asmecher Exp $

class Currency extends DataObject {

	function Currency() {
		parent::DataObject();
	}

	//
	// Get/set methods
	//

	/**
	 * Get the name of the currency.
	 * @return string
	 */
	function getName() {
		return $this->getData('name');
	}

	/**
	 * Set the name of the currency.
	 * @param $name string
	 */
	function setName($name) {
		return $this->setData('name', $name);
	}

	/**
	 * Get currency alpha code.
	 * @return string 
	 */
	function getCodeAlpha() {
		return $this->getData('codeAlpha');
	}

	/**
	 * Set currency alpha code.
	 * @param $alphaCode string
	 */
	function setCodeAlpha($codeAlpha) {
		return $this->setData('codeAlpha', $codeAlpha);
	}

	/**
	 * Get currency numeric code.
	 * @return int 
	 */
	function getCodeNumeric() {
		return $this->getData('codeNumeric');
	}

	/**
	 * Set currency numeric code.
	 * @param $codeNumeric string
	 */
	function setCodeNumeric($codeNumeric) {
		return $this->setData('codeNumeric', $codeNumeric);
	}
}

?>
