<?php

/**
 * @file GenericPlugin.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class GenericPlugin
 * @ingroup plugins
 *
 * @brief Abstract class for generic plugins
 */

//$Id: GenericPlugin.inc.php,v 1.7.2.1 2009/04/08 20:45:42 asmecher Exp $

import('plugins.Plugin');

class GenericPlugin extends Plugin {
	// No additional functions for now.
}

?>
