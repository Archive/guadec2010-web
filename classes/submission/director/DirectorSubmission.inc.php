<?php

/**
 * @file DirectorSubmission.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class DirectorSubmission
 * @ingroup submission
 * @see DirectorSubmissionDAO
 *
 * @brief DirectorSubmission class.
 */

//$Id: DirectorSubmission.inc.php,v 1.5.2.1 2009/04/08 20:45:42 asmecher Exp $

import('submission.trackDirector.TrackDirectorSubmission');

class DirectorSubmission extends TrackDirectorSubmission {

	/**
	 * Constructor.
	 */
	function DirectorSubmission() {
		parent::TrackDirectorSubmission();
	}
}

?>
